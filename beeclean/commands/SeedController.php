<?php

namespace app\commands;

use Yii;
use yii\console\Controller;
use app\models\User;
use app\models\AuthItem;
use app\models\AuthAssignment;
use app\models\Page;
use app\models\Service;

class SeedController extends Controller
{
    public function actionReset()
    {
        d('Reseting all models');
        $connection = Yii::$app->db;
        $connection->createCommand("SET FOREIGN_KEY_CHECKS = 0")->execute();
        foreach ($connection->getSchema()->getTableNames() as $tableName)
        {
            if ('migration' === $tableName)
                continue;

            $connection->createCommand("TRUNCATE TABLE `$tableName`")->execute();
        }
        $connection->createCommand("SET FOREIGN_KEY_CHECKS = 1")->execute();
    }

    public function actionIndex()
    {
        d('Running all seeds');
        $this->actionUsers();
        $this->actionServices();
    }

    public function actionUsers()
    {
        d('Seeding User model');

        $auth = new AuthItem(['name' => 'admin', 'type' => 1]);
        $auth->save();    
        $user = new User(['login' => 'Admin', 'email' => 'admin@beeclean.com', 'password' => 'admin', 'phone' => '0987654321']);
        $user->generateSeedAuthKey();
        $user->save();
        $assignment = new AuthAssignment(['item_name' => $auth->name, 'user_id' => "$user->id"]);
        $assignment->save();
    }

    public function actionServices()
    {
        d('Seeding Service model');

        (new Service(['name' => 'Помыть посуду', 'price' => 0]))->save();
        (new Service(['name' => 'Сменить коту лоток', 'price' => 0]))->save();
        (new Service(['name' => 'Помыть окна', 'price' => 0]))->save();
        (new Service(['name' => 'Помыть духовку', 'price' => 0]))->save();
    }
}
