<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\Url;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\About;
use app\models\Price;
use app\models\Post;
use app\models\Order;
use app\models\Address;
use app\models\SignupForm;
use app\models\Service;
use app\models\OrderServices;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $this->layout = 'main2';
        $prices = Price::find()->asArray()->all();
        return $this->render('index', ['prices' => $prices]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            if(Yii::$app->user->can('admin'))
            {
                return $this->redirect(['admin/user/index']);
            }else
            {
                return $this->goBack();
            }
        }
        return $this->redirect('index');
    }

    public function actionSignup()
    {
        
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->user->login($user)) {
                    return $this->redirect(Url::toRoute(['site/dashboard']));
                }
            }
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionBecomeCleaner()
    {
        if(Yii::$app->request->post())
        {
            $full_name = Yii::$app->request->post('full_name');
            $birth_date = Yii::$app->request->post('birth_date');
            $phone = Yii::$app->request->post('phone');

            Yii::$app->mailer->compose('contact_me', compact('full_name', 'birth_date', 'phone', 'body'))
            ->setFrom('noReply@beeclean.com')
            ->setTo(Yii::$app->params['adminEmail'])
            ->setSubject('Хочу быть частью команды Beeclean')
            ->send();
        }
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        $this->layout = 'main2';
        $model = About::findOne(1);
        return $this->render('about', ['model' => $model]);
    }

    /**
     * Displays becleaner page.
     *
     * @return string
     */
    public function actionBecleaner()
    {
        return $this->render('becleaner');
    }

    /**
     * Displays blog page.
     *
     * @return string
     */
    public function actionBlog()
    {
        $posts = Post::find()->all();
        return $this->render('blog', ['posts' => $posts]);
    }

    /**
     * Displays copyright page.
     *
     * @return string
     */
    public function actionCopyright()
    {
        return $this->render('copyright');
    }

    /**
     * Displays dashboard-single-item page.
     *
     * @return string
     */
    public function actionDashboardSingleItem()
    {
        if(!Yii::$app->user->isGuest)
        {
            $prices = Price::find()->asArray()->all();
            return $this->render('dashboard-single-item', ['prices' => $prices]);
        }else
        {
            $this->redirect('index');
        }
    }

    /**
     * Displays dashboard page.
     *
     * @return string
     */
    public function actionDashboard()
    {
        if(!Yii::$app->user->isGuest)
        {
            $prices = Price::find()->asArray()->all();
            return $this->render('dashboard', ['prices' => $prices]);
        }else
        {
            $this->redirect('index');
        }
    }

    /**
     * Displays faq page.
     *
     * @return string
     */
    public function actionFaq()
    {
        return $this->render('faq');
    }

    /**
     * Displays order-form page.
     *
     * @return string
     */
    public function actionOrderForm()
    {
        $this->layout = 'main2';
        $prices = Price::find()->asArray()->all();
        $services = Service::find()->asArray()->all();
        return $this->render('order-form', ['prices' => $prices, 'services' => $services]);
    }

    /**
     * Displays sitemap page.
     *
     * @return string
     */
    public function actionSitemap()
    {
        return $this->render('sitemap');
    }

    public function actionOrder()
    {
        $order = new Order();
        $address = new Address();
        if(!Yii::$app->user->isGuest && $order->load(Yii::$app->request->post()) && $address->load(Yii::$app->request->post()))
        {
            $address->user_id = Yii::$app->user->identity->id;
            $address->save();

            $order->address_id = $address->id;
            $order->date_time = (new \DateTime())->format('Y-m-d H-i-s');
            $price = Price::find()->where(['bathrooms' => $address->bathrooms, 'rooms' => $address->rooms])->one();
            $order->cost = $price->price;
            $order->status = 'Новый';
            $order->is_regular = (isset($order->is_regular))?1:0;
            $order->save();
            foreach ($order->services as $key => $value) 
            {
                $service = Service::findOne($key);
                (new OrderServices(['order_id' => $order->id, 'service_id' => $service->id]))->save();
                $order->cost += $service->price;
            }
            $order->save();
            $this->redirect('dashboard');
        }else
        {
            $this->redirect(Yii::$app->request->referrer);
        }
    }
}
