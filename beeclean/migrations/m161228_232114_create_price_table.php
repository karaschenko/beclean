<?php

use yii\db\Migration;

/**
 * Handles the creation of table `price`.
 */
class m161228_232114_create_price_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('price', [
            'id' => $this->primaryKey(),
            'bathrooms' => $this->integer()->notNull(),
            'rooms' => $this->integer()->notNull(),
            'price' => $this->integer()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('price');
    }
}
