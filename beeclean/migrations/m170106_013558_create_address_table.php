<?php

use yii\db\Migration;

/**
 * Handles the creation of table `address`.
 * Has foreign keys to the tables:
 *
 * - `user`
 */
class m170106_013558_create_address_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('address', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'phone' => $this->string(),
            'email' => $this->string(),
            'street' => $this->string()->notNull(),
            'house' => $this->string()->notNull(),
            'apartment' => $this->string()->notNull(),
            'city' => $this->string()->NotNull(),
            'country' => $this->string()->notNull(),
            'square' => $this->integer(),
            'rooms' => $this->integer()->notNull(),
            'bathrooms' => $this->integer()->notNull(),
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            'idx-address-user_id',
            'address',
            'user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-address-user_id',
            'address',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-address-user_id',
            'address'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            'idx-address-user_id',
            'address'
        );

        $this->dropTable('address');
    }
}
