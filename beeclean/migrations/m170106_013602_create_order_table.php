<?php

use yii\db\Migration;

/**
 * Handles the creation of table `order`.
 * Has foreign keys to the tables:
 *
 * - `address`
 */
class m170106_013602_create_order_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('order', [
            'id' => $this->primaryKey(),
            'address_id' => $this->integer()->notNull(),
            'first_name' => $this->string()->notNull(),
            'middle_name' => $this->string(),
            'last_name' => $this->string(),
            'date_time' => $this->dateTime()->notNull(),
            'editional_info' => $this->text(),
            'is_regular' => $this->boolean()->notNull()->defaultValue(false),
            'regular_description' => $this->text(),
            'cost' => $this->double()->notNull(),
        ]);

        // creates index for column `address_id`
        $this->createIndex(
            'idx-order-address_id',
            'order',
            'address_id'
        );

        // add foreign key for table `address`
        $this->addForeignKey(
            'fk-order-address_id',
            'order',
            'address_id',
            'address',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `address`
        $this->dropForeignKey(
            'fk-order-address_id',
            'order'
        );

        // drops index for column `address_id`
        $this->dropIndex(
            'idx-order-address_id',
            'order'
        );

        $this->dropTable('order');
    }
}
