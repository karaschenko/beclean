<?php

use yii\db\Migration;

/**
 * Handles adding status to table `order`.
 */
class m170111_235635_add_status_column_to_order_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('order', 'status', $this->string()->notNull());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('order', 'status');
    }
}
