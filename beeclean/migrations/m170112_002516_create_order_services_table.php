<?php

use yii\db\Migration;

/**
 * Handles the creation of table `order_services`.
 * Has foreign keys to the tables:
 *
 * - `order`
 * - `service`
 */
class m170112_002516_create_order_services_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('order_services', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->notNull(),
            'service_id' => $this->integer()->notNull(),
        ]);

        // creates index for column `order_id`
        $this->createIndex(
            'idx-order_services-order_id',
            'order_services',
            'order_id'
        );

        // add foreign key for table `order`
        $this->addForeignKey(
            'fk-order_services-order_id',
            'order_services',
            'order_id',
            'order',
            'id',
            'CASCADE'
        );

        // creates index for column `service_id`
        $this->createIndex(
            'idx-order_services-service_id',
            'order_services',
            'service_id'
        );

        // add foreign key for table `service`
        $this->addForeignKey(
            'fk-order_services-service_id',
            'order_services',
            'service_id',
            'service',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `order`
        $this->dropForeignKey(
            'fk-order_services-order_id',
            'order_services'
        );

        // drops index for column `order_id`
        $this->dropIndex(
            'idx-order_services-order_id',
            'order_services'
        );

        // drops foreign key for table `service`
        $this->dropForeignKey(
            'fk-order_services-service_id',
            'order_services'
        );

        // drops index for column `service_id`
        $this->dropIndex(
            'idx-order_services-service_id',
            'order_services'
        );

        $this->dropTable('order_services');
    }
}
