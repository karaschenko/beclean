<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "address".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $phone
 * @property string $email
 * @property string $street
 * @property string $house
 * @property string $apartment
 * @property string $city
 * @property string $country
 * @property integer $square
 * @property integer $rooms
 * @property integer $bathrooms
 *
 * @property User $user
 * @property Order[] $orders
 */
class Address extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'address';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'street', 'house', 'apartment', 'city', 'country', 'rooms', 'bathrooms'], 'required'],
            [['user_id', 'square', 'rooms', 'bathrooms'], 'integer'],
            [['phone', 'email', 'street', 'house', 'apartment', 'city', 'country'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'phone' => 'Phone',
            'email' => 'Email',
            'street' => 'Street',
            'house' => 'House',
            'apartment' => 'Apartment',
            'city' => 'City',
            'country' => 'Country',
            'square' => 'Square',
            'rooms' => 'Rooms',
            'bathrooms' => 'Bathrooms',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['address_id' => 'id']);
    }
}
