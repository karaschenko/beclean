<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order".
 *
 * @property integer $id
 * @property integer $address_id
 * @property string $first_name
 * @property string $middle_name
 * @property string $last_name
 * @property string $date_time
 * @property string $editional_info
 * @property integer $is_regular
 * @property string $regular_description
 * @property double $cost
 * @property string $status
 *
 * @property Address $address
 * @property OrderServices[] $orderServices
 */
class Order extends \yii\db\ActiveRecord
{
    public $services;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['address_id', 'first_name', 'date_time', 'cost', 'status'], 'required'],
            [['address_id', 'is_regular'], 'integer'],
            [['date_time', 'services'], 'safe'],
            [['editional_info', 'regular_description'], 'string'],
            [['cost'], 'number'],
            [['first_name', 'middle_name', 'last_name', 'status'], 'string', 'max' => 255],
            [['address_id'], 'exist', 'skipOnError' => true, 'targetClass' => Address::className(), 'targetAttribute' => ['address_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'address_id' => 'Address ID',
            'first_name' => 'Имя',
            'middle_name' => 'Отчество',
            'last_name' => 'Фамилия',
            'date_time' => 'Дата создания заказа',
            'editional_info' => 'Дополнительная информация',
            'is_regular' => 'Регулярный заказ',
            'regular_description' => 'Описание регулярного заказа',
            'cost' => 'Стоимость',
            'status' => 'Статус',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddress()
    {
        return $this->hasOne(Address::className(), ['id' => 'address_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderServices()
    {
        return $this->hasMany(OrderServices::className(), ['order_id' => 'id']);
    }
}
