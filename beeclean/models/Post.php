<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "post".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $seo_title
 * @property string $seo_description
 * @property string $seo_keywords
 * @property string $image_path
 */
class Post extends \yii\db\ActiveRecord
{
    public $image_file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['image_file'], 'required', 'on'=>'create'],
            [['description', 'seo_description', 'seo_keywords'], 'string'],
            [['title', 'seo_title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'description' => 'Страница',
            'seo_title' => 'Seo Title',
            'seo_description' => 'Seo Description',
            'seo_keywords' => 'Seo Keywords',
            'image_path' => 'Адрес картинки',
        ];
    }
}
