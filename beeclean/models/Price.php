<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "price".
 *
 * @property integer $id
 * @property integer $bathrooms
 * @property integer $rooms
 * @property integer $price
 */
class Price extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'price';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bathrooms', 'rooms', 'price'], 'required'],
            [['bathrooms', 'rooms', 'price'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bathrooms' => 'Количество санузлов',
            'rooms' => 'Количество комнат',
            'price' => 'Цена, грн.',
        ];
    }
}
