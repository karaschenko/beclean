<?php

namespace app\models;

use Yii;
use yii\base\Model;

class SignupForm extends Model
{
	public $email;
	public $password;
	public $login;
    public $phone;

	public function rules()
	{
		return [
			[['email'], 'email'],
            [['email', 'password', 'login', 'phone'], 'string', 'max' => 255],
            [['email', 'password', 'login', 'phone'], 'required'],
			[['password'], 'string', 'min' => 6, 'tooShort' => 'Пароль должен быть не меньше 6 символов'],
            ['email', 'unique', 'targetClass' => '\app\models\User', 'message' => 'Этот email уже используется'],
            ['phone', 'unique', 'targetClass' => '\app\models\User', 'message' => 'Этот телефон уже инпользуется'],
		];
	}

	 public function signup()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = new User();
        $user->email = $this->email;
        $user->login = $this->login;
        $user->phone = $this->phone;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->save();
        if ($user->save())
        {
            return $user;
        }
        return false;
    }


}
?>
