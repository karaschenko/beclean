<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $login
 * @property string $password_hash
 * @property string $auth_key
 * @property string $password_reset_token
 * @property string $phone
 * @property string $email
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    public $role;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login', 'password_hash', 'auth_key', 'email'], 'required'],
            [['login', 'password_hash', 'auth_key', 'password_reset_token', 'phone', 'email'], 'string', 'max' => 255],
            [['role'], 'safe'],
            [['login'], 'unique'],
            [['email'], 'unique'],
            [['password_reset_token'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'login' => 'Логин',
            // 'password_hash' => 'Password Hash',
            // 'auth_key' => 'Auth Key',
            // 'password_reset_token' => 'Password Reset Token',
            'phone' => 'Телефон',
            'email' => 'Email',
        ];
    }


    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return User::findOne($id);
    }

    /**
     * @return string hash of password
     */
    public function hashPassword($password)
    {
        return md5(Yii::$app->params['user.passwordSalt'].trim($password).Yii::$app->params['user.passwordSalt']);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = $this->hashPassword($password);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */

    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }

    public static function findByLogin($login)
    {
        return static::findOne(['login' => $login]);
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = md5(uniqid()) . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = md5(uniqid());
    }

    /**
     * Generates "remember me" authentication key for seeding
     */
    public function generateSeedAuthKey()
    {
        $this->auth_key = md5(uniqid());
        return $this; 
    }

    public function setDate()
    {

        $date = new \DateTime();
        $datestring = $date->format('Y-m-d');
        $this->regestration_date = $datestring;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->hashPassword($password) === $this->password_hash;
    }

    public function getAddresses()
    {
        return $this->hasMany(Address::className(), ['user_id' => 'id']);
    }

    public function isAdmin()
    {
        return (AuthAssignment::find()->where(['user_id' => "$this->id"])->one() != null)?true:false;
    }
}
