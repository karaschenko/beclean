<?php

namespace app\modules\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\helpers\Url;

/**
 * PageController implements the CRUD actions for Page model.
 */
class PageController extends Controller
{
    public function actionUploader()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $file = UploadedFile::getInstanceByName('upload');
        if (!file_exists('uploads'))
        {
            mkdir('uploads');
        }
        if (!file_exists('uploads/editor_images'))
        {
            mkdir('uploads/editor_images');
        }
        $date = new \DateTime();
        $timeString = $date->format('Y-m-d H-i-s');
        if($file->saveAs('uploads/editor_images/' . $file->baseName.$timeString . '.' . $file->extension))
        {
            $result = ["uploaded" => 1,
                "fileName" => $file->baseName.$timeString . '.' . $file->extension,
                "url" => Url::to('@web/uploads/editor_images/' . $file->baseName.$timeString . '.' . $file->extension),
            ];
        }else
        {
            $result = ["uploaded" => 0,
                'error'=> ['message' => 'Не можливо завантажити цей файл'],
            ];
        }
        return $result;
    }
}
