<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Post;
use app\modules\admin\models\search\PostSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * PostController implements the CRUD actions for Post model.
 */
class PostController extends Controller
{
    /**
     * Lists all Post models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PostSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Post model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Post model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Post(['scenario' => 'create']);

        if ($model->load(Yii::$app->request->post())) {
            $model->image_file = UploadedFile::getInstance($model, 'image_file');
            if(isset($model->image_file))
            {
                if (!file_exists('uploads'))
                {
                    mkdir('uploads');
                }
                if (!file_exists('uploads/blog_images'))
                {
                    mkdir('uploads/blog_images');
                }
                $date = new \DateTime();
                $timeString = $date->format('Y-m-d H-i-s');
                $model->image_path = 'uploads/blog_images/' . $model->image_file->baseName.$timeString . '.' . $model->image_file->extension;
                $model->image_file->saveAs('uploads/blog_images/' . $model->image_file->baseName.$timeString . '.' . $model->image_file->extension);
                $model->date = $date->format('Y-m-d');
            }
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Post model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if(Yii::$app->user->can('admin'))
        {
            $model = $this->findModel($id);

            if ($model->load(Yii::$app->request->post())) {
                $model->image_file = UploadedFile::getInstance($model, 'image_file');
                if(isset($model->image_file))
                {
                    unlink($model->image_path);
                    $date = new \DateTime();
                    $timeString = $date->format('Y-m-d H-i-s');
                    $model->image_path = 'uploads/news_images/' . $model->image_file->baseName.$timeString . '.' . $model->image_file->extension;
                    $model->image_file->saveAs('uploads/news_images/' . $model->image_file->baseName.$timeString . '.' . $model->image_file->extension);
                }
                $slug = preg_replace('@[\s!:;_\?=\\\+\*/%&#]+@', '-', $model->slug);
                      //this will replace all non alphanumeric char with '-'
                $slug = mb_strtolower($slug);
                      //convert string to lowercase
                $slug = trim($slug, '-');
                      //trim whitespaces
                $model->slug = $slug;
                $model->save();
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }else
        {
            throw new ForbiddenHttpException('Ви не маєте прав доступу до цього розділу.');   
        }
    }

    /**
     * Deletes an existing Post model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Post model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Post the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Post::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
