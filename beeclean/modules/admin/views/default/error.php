<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="site-error">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="alert alert-danger">
        <?= nl2br(Html::encode($message)) ?>
    </div>

    <p>
        Ця помилка виникла під час обробки сервером вашого запросу.
    </p>
    <p>
        Будьласка зв'яжіться з нами, ящо ви вважаєте, що це помилка серверу. Дякуємо.
    </p>

</div>
