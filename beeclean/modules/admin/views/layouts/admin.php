<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use kartik\sidenav\SideNav;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap clearfix">
    <?php
    NavBar::begin([
        'brandLabel' => Html::img('@web/img/logo.png', ['width'=>'40', 'margin'=>'0', 'pading'=>'0']),
        'brandUrl' => Url::toRoute(['/admin']),
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['default/login']]
            ) : (
                '<li>'
                . Html::beginForm(['default/logout'], 'post', ['class' => 'navbar-form'])
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->login . ')',
                    ['class' => 'btn btn-link']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();


    ?>


    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
    </div>
    <?php if(!Yii::$app->user->isGuest && Yii::$app->user->can('admin')): ?>
    <div class="col-lg-2">
        <?= SideNav::widget([
            'type' => SideNav::TYPE_DEFAULT,

            'options'=>['containerOptions'=>['style'=>['width'=>'100', 'margin'=>'0', 'pading'=>'0'],],],
            'heading' => 'Панель администратора',
            'items' =>[
                ['label' => "Пользователи", 'url' => Url::toRoute(['user/index']), 'icon' => 'user', 'active' => (Yii::$app->controller->id == 'user')],
                ['label' => "О сервисе", 'url' => Url::toRoute(['about/index']), 'icon' => 'shopping-cart', 'active' => (Yii::$app->controller->id == 'about')],
                ['label' => "Цены", 'url' => Url::toRoute(['price/index']), 'icon' => 'bookmark', 'active' => (Yii::$app->controller->id == 'price')],
                ['label' => "Блог", 'url' => Url::toRoute(['post/index']), 'icon' => 'gift', 'active' => (Yii::$app->controller->id == 'post')],
                ['label' => "Заказы", 'url' => Url::toRoute(['order/index']), 'icon' => 'list', 'active' => (Yii::$app->controller->id == 'order')],
                ['label' => "Регулярные заказы", 'url' => Url::toRoute(['regular/index']), 'icon' => 'barcode', 'active' => (Yii::$app->controller->id == 'regular')],
                ['label' => "Сервисы", 'url' => Url::toRoute(['service/index']), 'icon' => 'wrench', 'active' => (Yii::$app->controller->id == 'service')],
            ],
        ]) ?>
    </div>
    <?php endif; ?>
   <div class="col-lg-1"></div>
    <div class="col-lg-8">
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
