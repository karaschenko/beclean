<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Order */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Заказы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    *[data-popup-btn]:hover {cursor: help;}
    .popup {display: none;position: fixed;background-color: #fff;padding: 10px;border: 1px #ddd solid;border-radius: 10px;  }
    .popup .table td:first-child {font-weight: 600;}
    .popup .table {margin: 0;}
    .popup.active {display: block;}
</style>

<div class="order-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить данный заказ?',
                'method' => 'post',
            ],
        ]) ?>
        <a class="btn btn-info" data-popup-btn="1">Владелец заказа</a>
        <a class="btn btn-info" data-popup-btn="2">Адрес заказа</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'address_id',
            'first_name',
            'middle_name',
            'last_name',
            'date_time',
            'editional_info:ntext',
            'is_regular',
            'regular_description:ntext',
            'cost',
            'status',
        ],
    ]) ?>

</div>

<!-- Сюда впихиваем данные  "Владелец заказа"-->
<div class="popup" data-popup-view="1">
    <table class="table table-striped table-bordered">
        <tr>
            <td>Логин</td>
            <td><?= $model->address->user->login; ?></td>
        </tr>
        <tr>
            <td>Телефон</td>
            <td><?= $model->address->user->phone; ?></td>
        </tr>
        <tr>
            <td>Email</td>
            <td><?= $model->address->user->email; ?></td>
        </tr>
    </table>
</div>


<!-- Сюда впихиваем данные  "Адрес заказа"-->
<div class="popup" data-popup-view="2">
    <table class="table table-striped table-bordered">
        <tr>
            <td>Страна</td>
            <td><?= $model->address->country; ?></td>
        </tr>
        <tr>
            <td>Город</td>
            <td><?= $model->address->city; ?></td>
        </tr>
        <tr>
            <td>Улица</td>
            <td><?= $model->address->street; ?></td>
        </tr>
        <tr>
            <td>Дом</td>
            <td><?= $model->address->house; ?></td>
        </tr>
        <tr>
            <td>Квартира</td>
            <td><?= $model->address->apartment; ?></td>
        </tr>
        <tr>
            <td>Телефон</td>
            <td><?= $model->address->phone; ?></td>
        </tr>
        <tr>
            <td>Email</td>
            <td><?= $model->address->email; ?></td>
        </tr>
        <tr>
            <td>Площадь</td>
            <td><?= $model->address->square; ?></td>
        </tr>
        <tr>
            <td>Комнат</td>
            <td><?= $model->address->rooms; ?></td>
        </tr>
        <tr>
            <td>Санузлов</td>
            <td><?= $model->address->bathrooms; ?></td>
        </tr>
    </table>
</div>

<script>
    //Здесь творятся чудеса! )0)))
    var b = document.querySelectorAll('*[data-popup-btn]'),
        v = document.querySelectorAll('*[data-popup-view]');

    for (var i = 0; i < b.length; i++) {
        b[i].addEventListener('mouseover', showPopup);
        b[i].addEventListener('mouseout', hidePopup);
    }

    function showPopup(e){
        var id = this.getAttribute('data-popup-btn')-1;
        v[id].classList.add('active');

        movePopup(e);
        this.onmousemove = movePopup;

        function movePopup(e){
            v[id].setAttribute('style',
                'top:' + parseInt(e.clientY + 20) + 'px;'+
                'left:' + parseInt(e.clientX + 20) + 'px;'
            );
        }
    }

    function hidePopup(){
        var id = this.getAttribute('data-popup-btn')-1;
        v[id].classList.remove('active');
        this.onmousemove = null;
    }
</script>