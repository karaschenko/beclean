<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\models\Price;
use yii\helpers\Html;
use app\assets\AppAsset;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

AppAsset::register($this);
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="<?=  Url::to('@web/css/main.css')?>">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>

    <script src="<?=  Url::to('@web/js/main.js')?>"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
</head>
<body>
<header class="header static container-fluid">
    <div class="container">
        <a href="<?= Url::to(['site/index']); ?>" class="header-logo"></a>

        <ul class="header-menu hidden-xs">
            <li class="header-menu-item"><a href="<?= Url::to(['site/about']); ?>">О сервисе</a></li>
            <li class="header-menu-item"><a href="<?= Url::to(['site/faq']); ?>">Вопросы и ответы</a></li>
            <li class="header-menu-item" style="display: none;"><a href="#" id="login_btn">Войти</a></li>
            <?php if(!Yii::$app->user->isGuest): ?>
                <li class="header-menu-item">
                    <?php ActiveForm::begin(['action' => ['site/logout']]); ?>
                        <button type="submit">Выйти (<span><?= Yii::$app->user->identity->login; ?></span>)</button>
                    <?php ActiveForm::end(); ?>
                </li>
            <?php endif; ?>
        </ul>

        <div class="header-dropdown visible-xs">
            <button class="header-dropdown-btn btn " type="button" id="button-menu">
                <span class="material-icons">&#xE5D2;</span>
            </button>
            <ul class="header-dropdown-menu">
                <li ><a href="<?= Url::to(['site/about']); ?>">О сервисе</a></li>
                <li><a href="<?= Url::to(['site/faq']); ?>">Вопросы и ответы</a></li>
                <li style="display: none;"><a href="#" id="login_btn_small">Войти</a></li>
                <?php if(!Yii::$app->user->isGuest): ?>
                    <li>
                        <?php ActiveForm::begin(['action' => ['site/logout']]); ?>
                            <button>Выйти (<span><?= Yii::$app->user->identity->login; ?></span>)</button>
                        <?php ActiveForm::end(); ?>
                    </li>
                <?php endif; ?>
            </ul>
        </div>
        <div class="ui-ic header-tel" data-ic="&#xE0B0;">(099) 937 9992</div>
    </div>
</header>

<?= $content; ?>

<footer class="footer" data-parallax="scroll"  data-image-src="<?=  Url::to('@web/img/section-7.jpg')?>">
    <section class="section-seventh container-fluid">
        <div class="row">
            <h1 class="section-seventh__title ui_tac">
                Уборка квартиры от 3000 грн.
            </h1>
        </div>
        <div class="row">
            <div class="section-seventh__description col-md-4 col-xs-12 col-sm-8 col-sm-offset-2 col-md-offset-4 ui_tac">
                Мы готовы приехать к вам уже сегодня
            </div>
        </div>

    </section>
    <section class="main-footer container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-3 col-md-3 col-sm-offset-1 col-sm-5 col-xs-12 ">

                    <h2 class="main-footer__title">BeeClean</h2>

                    <ul class="main-footer-list">
                        <li class="main-footer-list__item"><a href="<?= Url::to(['site/about']); ?>">О сервисе</a></li>
                        <li class="main-footer-list__item"><a href="<?= Url::to(['site/faq']); ?>">Вопросы и ответы</a></li>
                        <li class="main-footer-list__item"><a href="<?= Url::to(['site/blog']); ?>">Блог</a></li>
                        <li class="main-footer-list__item"><a href="<?= Url::to(['site/becleaner']); ?>">Работа в BeeClean</a></li>
                        <li class="main-footer-list__item"><a href="<?= Url::to(['site/sitemap']); ?>">Карта сайта</a></li>
                    </ul>

                </div>

                <div class=" col-md-3 col-sm-5  col-xs-12 ">
                    <h2 class="main-footer__title">Свяжитесь с нами</h2>

                    <ul class="main-footer-contacts">
                        <li class="main-footer-contacts__item ui-ic" data-ic="&#xE0BE;">feedback@beclean.com</li>
                        <li class="main-footer-contacts__item ui-ic" data-ic="&#xE0B0;">(099) 937 9992</li>

                    </ul>
                    <h2 class="main-footer__title">Свяжитесь с нами</h2>
                    <ul class="main-footer-social">
                        <li class="main-footer-social-item fb"><a href="#"></a></li>
                        <li class="main-footer-social-item tl"><a href="#"></a></li>
                        <li class="main-footer-social-item is"><a href="#"></a></li>
                        <li class="main-footer-social-item vk"><a href="#"></a></li>
                    </ul>
                </div>


            </div>
            <div class="row">
                <div class="col-md-offset-3 col-md-3 main-footer-logo">
                    <img src="<?=  Url::to('@web/img/logo.png')?>" alt="" class="img-responsive">
                </div>
                <div class="col-md-3 main-footer-copyright">
                    @2016 Beclean
                </div>
            </div>
        </div>
    </section>


<div class="modal-layout" id="login">
    <div class="modal-layout__close"></div>
    <div class="modal-window">
        <div class="material-icons modal-close">&#xE5C9;</div>

        <div class="info-block-form">
            <ul class="tabs-control">
                <li class="tabs-control__item primary"><span>Логин</span></li><li class="tabs-control__item" ><span>Регистрация</span> </li>
            </ul>
            <ul class="tabs-list">

                    <li class="tabs-list__item active">
                        <?php ActiveForm::begin(['action' => ['site/login']]); ?>
                        <div class="info-block-form-item">
                            <button class="info-block-form-item__btn info-block-form-item__btn_minus material-icons" >&#xE7FD;</button>
                            <input name="LoginForm[login]" class="info-block-form-item__info ui_input tal" placeholder="Логин">

                        </div>
                        <div class="info-block-form-item">
                            <button class="info-block-form-item__btn info-block-form-item__btn_minus material-icons">&#xE0DA;</button>
                            <input name="LoginForm[password]" type="password" class="info-block-form-item__info ui_input tal" placeholder="Пароль">

                        </div>



                        <div class="info-block-form-item">
                            <button class="info-block-form-item__info ui_btn" type="submit">Войти</button>
                        </div>
                        <?php ActiveForm::end(); ?>

                    </li>


                    <li class="tabs-list__item">
                        <?php ActiveForm::begin(['action' => ['site/signup']]); ?>
                        <div class="info-block-form-item">
                            <button class="info-block-form-item__btn info-block-form-item__btn_minus material-icons" >&#xE7FD;</button>
                            <input name="SignupForm[login]" class="info-block-form-item__info ui_input tal" placeholder="Логин">

                        </div>
                        <div class="info-block-form-item">
                            <button class="info-block-form-item__btn info-block-form-item__btn_minus material-icons">&#xE0DA;</button>
                            <input type="password" name="SignupForm[password]" class="info-block-form-item__info ui_input tal" placeholder="Пароль">

                        </div>
                        <div class="info-block-form-item">
                            <button class="info-block-form-item__btn info-block-form-item__btn_minus material-icons">&#xE0DA;</button>
                            <input type="password" class="info-block-form-item__info ui_input tal" placeholder="Повторите пароль">

                        </div>
                        <div class="info-block-form-item">
                            <button class="info-block-form-item__btn info-block-form-item__btn_minus material-icons">&#xE0B0;</button>
                            <input name="SignupForm[phone]" class="info-block-form-item__info ui_input tal" placeholder="Номер телефона">

                        </div>
                        <div class="info-block-form-item">
                            <button class="info-block-form-item__btn info-block-form-item__btn_minus material-icons">&#xE0BE;</button>
                            <input name="SignupForm[email]" class="info-block-form-item__info ui_input tal" placeholder="Е-мейл">

                        </div>


                        <div class="info-block-form-item" >
                            <label  class="info-block-form-item__btn info-block-form-item__btn_minus material-icons">
                                <input type="checkbox" class="info-block-form-item__checkbox">
                            </label>
                            <div type="text" class="info-block-form-item__info ui_input tal ui_p"> Я прочел <a style="text-decoration: underline;" href="<?= Url::to(['site/copyright']); ?>">СОГЛАШЕНИЕ</a></div>
                        </div>



                        <div class="info-block-form-item">
                            <button class="info-block-form-item__info ui_btn" type="submit">Зарегистрироваться</button>
                        </div>
                        <?php ActiveForm::end(); ?>

                    </li>

            </ul>


        </div>
    </div>
</div>


    <div class="modal-layout" id="single_adres">
        <div class="modal-layout__close"></div>
        <div class="modal-window" style="min-width: 75%; max-width: 90%">
            <div class="material-icons modal-close">&#xE5C9;</div>

            <div class="info-block-form">
                <div class="dashboard-address" style="padding: 30px; border-radius: 15px;">
                    <div class="dashboard-address__title">
                        г. Днепр, пр. Слобожанский 32/2 кв.532
                    </div>
                    <div class="row dashboard-address__head dashboard-address-item">
                        <div class="col-md-2 col-xs-3">
                            Id
                        </div>
                        <div class="col-md-2 col-md-offset-1 col-xs-3">
                            Дата
                        </div>
                        <div class="col-md-2 col-md-offset-1 col-xs-3">
                            Цена
                        </div>
                        <div class="col-md-3 col-md-offset-1 col-xs-3">
                            Статус
                        </div>

                    </div>
                    <div class="row dashboard-address-item">
                        <div class="col-md-2 col-xs-3">
                            59
                        </div>
                        <div class="col-md-2 col-md-offset-1 col-xs-3">
                            01.02.2015
                        </div>
                        <div class="col-md-2 col-md-offset-1 col-xs-3">
                            2000грн
                        </div>
                        <div class="col-md-3 col-md-offset-1 done col-xs-3">
                            Выполнено
                        </div>
                    </div>
                    <div class="row dashboard-address-item">
                        <div class="col-md-2 col-xs-3 ">
                            59
                        </div>
                        <div class="col-md-2 col-md-offset-1 col-xs-3">
                            01.02.2015
                        </div>
                        <div class="col-md-2 col-md-offset-1 col-xs-3">
                            2000грн
                        </div>
                        <div class="col-md-3 col-md-offset-1 not-done col-xs-3">
                            Не выполнено
                        </div>
                    </div>
                    <div class="row dashboard-address-item">
                        <div class="col-md-2 col-xs-3">
                            59
                        </div>
                        <div class="col-md-2 col-md-offset-1 col-xs-3">
                            01.02.2015
                        </div>
                        <div class="col-md-2 col-md-offset-1 col-xs-3">
                            2000грн
                        </div>
                        <div class="col-md-3 col-md-offset-1 cancel col-xs-3">
                            Отменен
                        </div>
                    </div>

                    <ul class="dashboard-pagination">
                        <li class="dashboard-pagination__item material-icons navigation">&#xE314;</li>
                        <li class="dashboard-pagination__item">1</li>
                        <li class="dashboard-pagination__item active">2</li>
                        <li class="dashboard-pagination__item">3</li>
                        <li class="dashboard-pagination__item material-icons navigation">&#xE315;</li>

                    </ul>

                </div>
            </div>
        </div>
    </div>

</footer>


<script src="<?=  Url::to('@web/js/parallax.min.js')?>"></script>
</body>
</html>