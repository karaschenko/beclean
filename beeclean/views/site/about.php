<?php
use yii\helpers\Url;
?>
<section class="container-fluid section-first" data-parallax="scroll" data-image-src="<?=  Url::to('@web/img/about.jpg')?>" >
    <div class="container">
        <div class="row about">
            <div class="about__text col-md-6">
                <?= $model!=null?$model->body:''; ?>
            </div>
        </div>

    </div>
</section>
<section class="container-fluid contacts">
    <div class="container">
        <h1 class="ui_h1">Контактная информация</h1>
        <div class="row">
            <div class="col-md-4  col-xs-12 contacts-item">
                <span  class="material-icons">&#xE0BE;</span> <span class="contacts-item__text">mail@mail.ru</span>
            </div>
            <div class="col-md-4  col-xs-12 contacts-item">
                <span class="material-icons">&#xE0B0;</span> <span class="contacts-item__text">9379992</span>
            </div>
            <div class="col-md-4  col-xs-12 contacts-item">
                <span  class="material-icons">&#xE0C8;</span> <span class="contacts-item__text">ул. Example, 163 г. Днепр, 49000</span>
            </div>

        </div>

    </div>


</section>
<section class="container-fluid map">

</section>
<div class="container-fluid">
    <div class="container">
        <div class="col-md-4 col-xs-12 col-md-offset-4">
            <a href="<?= Url::to(['site/order-form']); ?>" style="display: inline-block; text-align: center;" class="section-fourth_btn ui_btn ui_btn_inverse">Подать заявку сейчас</a>
        </div>
    </div>
</div>



