﻿<?php
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
?>
<section class="container-fluid section-first" data-parallax="scroll" data-image-src="<?=  Url::to('@web/img/cleaner.jpg')?>" >
    <div class="container">

        <div class="info-block row">
            <div class="col-md-offset-2 col-md-4 col-xs-12  info-block-text">
                <div class="info-block-text-main half">
                    Ищем специалистов по уборке в  Днепре
                </div>
                <div class="info-block-text-additional">
                    Отправьте заявку и мы ответим вам в течении 24 часов
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="info-block-form">
                    <?php ActiveForm::begin(['action' => ['site/become-cleaner']]); ?>
                        <div class="info-block-form__title">Расскажите о себе</div>
                        <div class="info-block-form-item">
                            <button class="info-block-form-item__btn info-block-form-item__btn_minus material-icons" >&#xE7FD;</button>
                            <input name="full_name" type="text" class="info-block-form-item__info ui_input tal" placeholder="Ф.И.О.">

                        </div>
                        <div class="info-block-form-item">
                            <button class="info-block-form-item__btn info-block-form-item__btn_minus material-icons" >&#xE916;</button>
                            <input name="birth_date" type="text" class="info-block-form-item__info ui_input tal" placeholder="Дата рождения">

                        </div>
                        <div class="info-block-form-item">
                            <div class="info-block-form-item__btn info-block-form-item__btn_tel material-icons">&#xE0B0;</div>
                            <input name="phone" class="info-block-form-item__info ui_input tal" type="tel" pattern="\([0-9]{3}\)\s[0-9]{3}-[0-9]{2}-[0-9]{2}" name="phone" placeholder="(050) 121-34-57" title="Формат ввода (050) 121-34-57" required/>

                        </div>
                        <div class="info-block-form-item">
                            <button class="info-block-form-item__info ui_btn" type="submit">Отправить заявку</button>
                        </div>
                    <?php ActiveForm::end(); ?>
                </div>

            </div>
        </div>
        <a href="#section2" class="next-section hidden-xs">Наши преимущества</a>
    </div>
</section>
<section class="container-fluid section-second" id="section2">
    <div class="container">
        <div class="row benefits-list">
            <div class="col-md-4  col-xs-12 benefits-list-item">
                <div class="benefits-list-item__image material-icons">&#xE916;</div>
                <div class="benefits-list-item__title">Гибкий график</div>
                <div class="benefits-list-item__description">Вы решаете, где и за какую ценуделать уборку, а также можетерасписать свою занятость нанеделю вперед</div>
            </div>
            <div class="col-md-4  col-xs-12 benefits-list-item">
                <div class="benefits-list-item__image material-icons">&#xE80C;</div>
                <div class="benefits-list-item__title">Стандартное обучение</div>
                <div class="benefits-list-item__description">Если вы чего-то не умеете, мыобязательно вас научим набесплатных тренингах и курсах</div>
            </div>
            <div class="col-md-4  col-xs-12 benefits-list-item">
                <div class="benefits-list-item__image material-icons">&#xE227;</div>
                <div class="benefits-list-item__title">Регулярные выплаты</div>
                <div class="benefits-list-item__description">Вы всегда видите, сколько ужезаработали. Деньги поступаютна карту два раза в месяц</div>
            </div>

        </div>
        <div class="row benefits-list">
            <div class="col-md-4  col-xs-12 benefits-list-item">
                <div class="benefits-list-item__image material-icons">&#xE8DC;</div>
                <div class="benefits-list-item__title">Бесплатный инвентарь</div>
                <div class="benefits-list-item__description">Мы выдадим вам формуи все необходимое для уборки.Бесплатно пополнить запасыможно в офисе</div>
            </div>
            <div class="col-md-4  col-xs-12 benefits-list-item">
                <div class="benefits-list-item__image material-icons">&#xEB3C;</div>
                <div class="benefits-list-item__title">Выездная работа</div>
                <div class="benefits-list-item__description">Вы можете выбирать заказытолько рядом с вами исоставлять удобныйдля себя маршрут</div>
            </div>
            <div class="col-md-4  col-xs-12 benefits-list-item">
                <div class="benefits-list-item__image material-icons">&#xE32A;</div>
                <div class="benefits-list-item__title">Страховой фонд</div>
                <div class="benefits-list-item__description">Если что-то пойдет не так, вы неостанетесь один на один снеобходимостью покрытьклиенту ущерб</div>
            </div>
        </div>
    </div>
    <div class="col-md-4 col-xs-12 col-md-offset-4">
        <button class="section-fourth_btn ui_btn ui_btn_inverse">Подать заявку сейчас</button>
    </div>

</section>
