<?php
use yii\helpers\Url;
?>
<section class="blog container-fluid">
    <div class="container">
        <h1 class="blog-title ui_h1">
            Блог
        </h1>
        <div class="row hidden-sm hidden-xs">
        <?php foreach($posts as $post): ?>
            <a href ="<?= Url::to(['site/post', 'id' => $post->id]); ?>" class="blog-item">
                <div class="blog-item__img col-md-2 col-lg-2 col-xs-12">
                    <img src="<?= Url::to('@web/'.$post->image_path); ?>" class="img-responsive" alt="">
                </div>
                <div class="blog-item-content col-md-4 col-xs-12">
                    <div class="blog-item-content__title">
                        <?= $post->title; ?>
                    </div>
                    <div class="blog-item-content__excerpt">
                        И при этом сохранить бюджет
                    </div>
                    <div class="blog-item-content__date">
                        <?= $post->date; ?>
                    </div>
                </div>
            </a>
        <?php endforeach; ?>

        </div>
        <div class="row hidden-sm hidden-xs">
            <div class="col-md-4 col-xs-12 col-md-offset-4 ui_tac">
                <button class="section-sixth_btn ui_btn ">Показать еще</button>
            </div>
        </div>

        <div class="swiper-container visible-xs visible-sm">
            <!-- Additional required wrapper -->
            <div class="swiper-wrapper">
                <!-- Slides -->
                <div class="swiper-slide ">
                    <a href ="#" class="blog-item">
                        <div class="blog-item__img col-md-2 col-lg-2 col-xs-12">
                            <img src="img/blog-img.png"  alt="">
                        </div>
                        <div class="blog-item-content col-md-4 col-xs-12">
                            <div class="blog-item-content__title">
                                6 способов заставить квартиру выглядеть достойно
                            </div>
                            <div class="blog-item-content__excerpt">
                                И при этом сохранить бюджет
                            </div>
                            <div class="blog-item-content__date">
                                16 августа
                            </div>

                        </div>

                    </a>
                </div>
                <div class="swiper-slide ">
                    <a href ="#" class="blog-item">
                        <div class="blog-item__img col-md-2 col-lg-2 col-xs-12">
                            <img src="img/blog-img.png"  alt="">
                        </div>
                        <div class="blog-item-content col-md-4 col-xs-12">
                            <div class="blog-item-content__title">
                                6 способов заставить квартиру выглядеть достойно
                            </div>
                            <div class="blog-item-content__excerpt">
                                И при этом сохранить бюджет
                            </div>
                            <div class="blog-item-content__date">
                                16 августа
                            </div>

                        </div>

                    </a>
                </div>
                <div class="swiper-slide ">
                    <a href ="#" class="blog-item">
                        <div class="blog-item__img col-md-2 col-lg-2 col-xs-12">
                            <img src="img/blog-img.png"  alt="">
                        </div>
                        <div class="blog-item-content col-md-4 col-xs-12">
                            <div class="blog-item-content__title">
                                6 способов заставить квартиру выглядеть достойно
                            </div>
                            <div class="blog-item-content__excerpt">
                                И при этом сохранить бюджет
                            </div>
                            <div class="blog-item-content__date">
                                16 августа
                            </div>

                        </div>

                    </a>
                </div>

            </div>


        </div>
    </div>
</section>