
<section class="copyright container-fluid">
    <div class="container">
        <h1 class="copyright__title ui_h1">
            Пользовательское соглашения
        </h1>
        <p class="ui_p ui_tac">Днепр, Украина, Версия 1.2 от 04 апреля 2016 г.</p>

        <div class="row">
            <div class="col-md-10 copyright__text col-md-offset-1">
                <p class="ui_p">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet aspernatur atque consequatur consequuntur corporis delectus dolores, ex facere fuga in iste pariatur possimus quas repudiandae, sequi tempora temporibus velit voluptatum.
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet consequatur corporis culpa, dignissimos dolores error id in libero minus modi necessitatibus officia pariatur perspiciatis placeat ratione recusandae saepe voluptate. Quaerat!
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur consequuntur dolor, eius eligendi eos est eveniet exercitationem in laboriosam neque odit porro praesentium, quia repudiandae rerum sapiente sunt veniam voluptate.
                </p>

                <h2 class="ui_h2">
                    1. Общие положения пользовательского соглашения
                </h2>
                <p class="ui_p">
                    1.1.	Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum
                </p>
                <p class="ui_p">1.2.	Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
                <h2 class="ui_h2">
                    1. Общие положения пользовательского соглашения
                </h2>
                <p class="ui_p">
                    1.1.	Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum
                </p>
                <p class="ui_p">1.2.	Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
                <h2 class="ui_h2">
                    1. Общие положения пользовательского соглашения
                </h2>
                <p class="ui_p">
                    1.1.	Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum
                </p>
                <p class="ui_p">1.2.	Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>

            </div>


        </div>
    </div>


</section>