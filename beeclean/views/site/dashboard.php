<?php
use yii\helpers\Url;
?>
<section class="dashboard container-fluid">
    <div class="container">
        <h1 class="dashboard__title ui_h1">
            Личный кабинет
        </h1>
        <div class="row">
            <ul class="tabs-control col-md-6 col-md-offset-3">
                <li class="tabs-control__item primary" ><span>Основное</span></li><li class="tabs-control__item" ><span>Адреса</span> </li>
            </ul>
        </div>

        <ul class="tabs-list">
            <li class="tabs-list__item active dashboard-content">
                <div class="row">
                    <div class="col-md-2">
                        <div class="dashboard-content__image" style="background-image: url('<?=  Url::to('@web/img/telo4ka.png')?>')">
                        </div>

                    </div>
                    <div class="col-md-4 col-md-offset-1">
                        <!-- <div class="dashboard-content__option">Имя</div>
                        <div class="dashboard-content__value">Мария Цымбал</div> -->
                        <div class="dashboard-content__option">Логин</div>
                        <div class="dashboard-content__value"><?= Yii::$app->user->identity->login; ?></div>
                        <div class="dashboard-content__option">Електронная почта</div>
                        <div class="dashboard-content__value"><?= Yii::$app->user->identity->email; ?></div>
                        <div class="dashboard-content__option">Телефон</div>
                        <div class="dashboard-content__value"><?= Yii::$app->user->identity->phone; ?></div>
                    </div>
                    <div class="col-md-4 col-md-offset-1">
                        <button class="ui_btn dashboard-content__btn" id="dashboard-change">Редактировать</button>
                        <button class="ui_btn dashboard-content__btn" id="password-change">Сменить пароль</button>
                    </div>

                </div>

            </li>
            <li class="tabs-list__item">
                <div class="dashboard-address">


                    <div class="row dashboard-address__head dashboard-address-item">
                        <div class="col-md-1 col-xs-2">
                            Id
                        </div>
                        <div class="col-md-11 col-xs-10">
                            Адресс
                        </div>
                    </div>


                    <!-- список адресов по 10 штук-->
                    <?php foreach(Yii::$app->user->identity->addresses as $address): ?>
                        <div class="row dashboard-address-item">
                            <a data-adress-modal="<?= $address->id; ?>" style="cursor: pointer">
                                <div class="col-md-1 col-xs-2"><?= $address->id; ?></div>
                                <div class="col-md-11 col-xs-10"><?= 'г. '.$address->city.', '.$address->street.' '.$address->house.' кв.'.$address->apartment; ?></div>
                            </a>
                        </div>
                    <?php endforeach; ?>

                    <!-- -->


                    <ul class="dashboard-pagination">
                        <li class="dashboard-pagination__item material-icons navigation">&#xE314;</li>
                        <li class="dashboard-pagination__item">1</li>
                        <li class="dashboard-pagination__item active">2</li>
                        <li class="dashboard-pagination__item">3</li>
                        <li class="dashboard-pagination__item material-icons navigation">&#xE315;</li>

                    </ul>

                </div>

            </li>
        </ul>

    </div>


</section>

<div class="modal-layout" id="change-data">
    <div class="modal-layout__close"></div>

    <div class="modal-window">
        <div class="material-icons modal-close">&#xE5C9;</div>
        <div class="info-block-form ">
            <div class="info-block-form-item">
                <button class="info-block-form-item__btn info-block-form-item__btn_minus material-icons" >&#xE7FD;</button>
                <input type="text" class="info-block-form-item__info ui_input tal" placeholder="Имя">

            </div>
            <div class="info-block-form-item">
                <button class="info-block-form-item__btn info-block-form-item__btn_minus material-icons">&#xE7FD;</button>
                <input type="text" class="info-block-form-item__info ui_input tal" placeholder="Фамилия">

            </div>
            <div class="info-block-form-item">
                <div class="info-block-form-item__btn info-block-form-item__btn_minus material-icons">&#xE0BE;</div>
                <input type="text" class="info-block-form-item__info ui_input tal" placeholder="Почта">

            </div>
            <div class="info-block-form-item">
                <div class="info-block-form-item__btn info-block-form-item__btn_minus material-icons">&#xE0B0;</div>
                <input type="text" class="info-block-form-item__info ui_input tal" placeholder="Телфон">
            </div>
            <div class="info-block-form-item">
                <div class="info-block-form-item__btn info-block-form-item__btn_minus material-icons">&#xE916;</div>
                <input type="text" class="info-block-form-item__info ui_input tal" placeholder="Адресс">
            </div>

            <div class="info-block-form-item">
                <button class="info-block-form-item__info ui_btn" type="submit">Сохранить</button>
            </div>
        </div>
    </div>
</div>
<div class="modal-layout" id="password-data">
    <div class="modal-layout__close"></div>

    <div class="modal-window">
        <div class="material-icons modal-close">&#xE5C9;</div>
        <div class="info-block-form ">
            <div class="info-block-form-item">
                <button class="info-block-form-item__btn info-block-form-item__btn_minus material-icons" >&#xE0DA;</button>
                <input type="password" class="info-block-form-item__info ui_input tal" placeholder="Старый пароль">

            </div>
            <div class="info-block-form-item">
                <button class="info-block-form-item__btn info-block-form-item__btn_minus material-icons">&#xE0DA;</button>
                <input type="password" class="info-block-form-item__info ui_input tal" placeholder="Новый пароль">

            </div>
            <div class="info-block-form-item">
                <div class="info-block-form-item__btn info-block-form-item__btn_minus material-icons">&#xE0DA;</div>
                <input type="password" class="info-block-form-item__info ui_input tal" placeholder="Новый пароль еще раз">

            </div>


            <div class="info-block-form-item">
                <button class="info-block-form-item__info ui_btn" type="submit">Сохранить</button>
            </div>
        </div>
    </div>
</div>


<script>
    window.onload = function(){

    }
</script>