<section class="faq container-fluid">
    <div class="container">
        <h1 class="fq__title ui_h1">
            Вопросы и ответы
        </h1>
        <div class="row faq-list">
            <div class="col-md-6">
                <h2 class="faq-list__title">
                    Уборка
                </h2>
                <ul class="faq-list-section">
                    <li class="faq-list-section-item">
                        <div class="faq-list-section-item__question">Из чего состои уборка?</div>
                        <div class="faq-list-section-item__answer">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis hic incidunt molestias quia rem soluta voluptas. Aliquid cupiditate ex impedit, magnam mollitia nam natus neque, nesciunt numquam provident, repellat sapiente.</div>
                    </li>
                    <li class="faq-list-section-item">
                        <div class="faq-list-section-item__question">Из чего состои уборка?</div>
                        <div class="faq-list-section-item__answer">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis hic incidunt molestias quia rem soluta voluptas. Aliquid cupiditate ex impedit, magnam mollitia nam natus neque, nesciunt numquam provident, repellat sapiente.</div>
                    </li>
                    <li class="faq-list-section-item">
                        <div class="faq-list-section-item__question">Из чего состои уборка?</div>
                        <div class="faq-list-section-item__answer">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis hic incidunt molestias quia rem soluta voluptas. Aliquid cupiditate ex impedit, magnam mollitia nam natus neque, nesciunt numquam provident, repellat sapiente.</div>
                    </li>
                    <li class="faq-list-section-item">
                        <div class="faq-list-section-item__question">Из чего состои уборка?</div>
                        <div class="faq-list-section-item__answer">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis hic incidunt molestias quia rem soluta voluptas. Aliquid cupiditate ex impedit, magnam mollitia nam natus neque, nesciunt numquam provident, repellat sapiente.</div>
                    </li>
                    <li class="faq-list-section-item">
                        <div class="faq-list-section-item__question">Из чего состои уборка?</div>
                        <div class="faq-list-section-item__answer">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis hic incidunt molestias quia rem soluta voluptas. Aliquid cupiditate ex impedit, magnam mollitia nam natus neque, nesciunt numquam provident, repellat sapiente.</div>
                    </li>
                </ul>
                <h2 class="faq-list__title">
                    Уборка
                </h2>
                <ul class="faq-list-section">
                    <li class="faq-list-section-item">
                        <div class="faq-list-section-item__question">Из чего состои уборка?</div>
                        <div class="faq-list-section-item__answer">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis hic incidunt molestias quia rem soluta voluptas. Aliquid cupiditate ex impedit, magnam mollitia nam natus neque, nesciunt numquam provident, repellat sapiente.</div>
                    </li>
                    <li class="faq-list-section-item">
                        <div class="faq-list-section-item__question">Из чего состои уборка?</div>
                        <div class="faq-list-section-item__answer">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis hic incidunt molestias quia rem soluta voluptas. Aliquid cupiditate ex impedit, magnam mollitia nam natus neque, nesciunt numquam provident, repellat sapiente.</div>
                    </li>
                    <li class="faq-list-section-item">
                        <div class="faq-list-section-item__question">Из чего состои уборка?</div>
                        <div class="faq-list-section-item__answer">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis hic incidunt molestias quia rem soluta voluptas. Aliquid cupiditate ex impedit, magnam mollitia nam natus neque, nesciunt numquam provident, repellat sapiente.</div>
                    </li>
                    <li class="faq-list-section-item">
                        <div class="faq-list-section-item__question">Из чего состои уборка?</div>
                        <div class="faq-list-section-item__answer">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis hic incidunt molestias quia rem soluta voluptas. Aliquid cupiditate ex impedit, magnam mollitia nam natus neque, nesciunt numquam provident, repellat sapiente.</div>
                    </li>
                    <li class="faq-list-section-item">
                        <div class="faq-list-section-item__question">Из чего состои уборка?</div>
                        <div class="faq-list-section-item__answer">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis hic incidunt molestias quia rem soluta voluptas. Aliquid cupiditate ex impedit, magnam mollitia nam natus neque, nesciunt numquam provident, repellat sapiente.</div>
                    </li>
                </ul>
            </div>
            <div class="col-md-6">
                <h2 class="faq-list__title">
                    Уборка
                </h2>
                <ul class="faq-list-section">
                    <li class="faq-list-section-item">
                        <div class="faq-list-section-item__question">Из чего состои уборка?</div>
                        <div class="faq-list-section-item__answer">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis hic incidunt molestias quia rem soluta voluptas. Aliquid cupiditate ex impedit, magnam mollitia nam natus neque, nesciunt numquam provident, repellat sapiente.</div>
                    </li>
                    <li class="faq-list-section-item">
                        <div class="faq-list-section-item__question">Из чего состои уборка?</div>
                        <div class="faq-list-section-item__answer">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis hic incidunt molestias quia rem soluta voluptas. Aliquid cupiditate ex impedit, magnam mollitia nam natus neque, nesciunt numquam provident, repellat sapiente.</div>
                    </li>
                    <li class="faq-list-section-item">
                        <div class="faq-list-section-item__question">Из чего состои уборка?</div>
                        <div class="faq-list-section-item__answer">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis hic incidunt molestias quia rem soluta voluptas. Aliquid cupiditate ex impedit, magnam mollitia nam natus neque, nesciunt numquam provident, repellat sapiente.</div>
                    </li>
                    <li class="faq-list-section-item">
                        <div class="faq-list-section-item__question">Из чего состои уборка?</div>
                        <div class="faq-list-section-item__answer">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis hic incidunt molestias quia rem soluta voluptas. Aliquid cupiditate ex impedit, magnam mollitia nam natus neque, nesciunt numquam provident, repellat sapiente.</div>
                    </li>
                    <li class="faq-list-section-item">
                        <div class="faq-list-section-item__question">Из чего состои уборка?</div>
                        <div class="faq-list-section-item__answer">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis hic incidunt molestias quia rem soluta voluptas. Aliquid cupiditate ex impedit, magnam mollitia nam natus neque, nesciunt numquam provident, repellat sapiente.</div>
                    </li>
                </ul>
                <h2 class="faq-list__title">
                    Уборка
                </h2>
                <ul class="faq-list-section">
                    <li class="faq-list-section-item">
                        <div class="faq-list-section-item__question">Из чего состои уборка?</div>
                        <div class="faq-list-section-item__answer">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis hic incidunt molestias quia rem soluta voluptas. Aliquid cupiditate ex impedit, magnam mollitia nam natus neque, nesciunt numquam provident, repellat sapiente.</div>
                    </li>
                    <li class="faq-list-section-item">
                        <div class="faq-list-section-item__question">Из чего состои уборка?</div>
                        <div class="faq-list-section-item__answer">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis hic incidunt molestias quia rem soluta voluptas. Aliquid cupiditate ex impedit, magnam mollitia nam natus neque, nesciunt numquam provident, repellat sapiente.</div>
                    </li>
                    <li class="faq-list-section-item">
                        <div class="faq-list-section-item__question">Из чего состои уборка?</div>
                        <div class="faq-list-section-item__answer">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis hic incidunt molestias quia rem soluta voluptas. Aliquid cupiditate ex impedit, magnam mollitia nam natus neque, nesciunt numquam provident, repellat sapiente.</div>
                    </li>
                    <li class="faq-list-section-item">
                        <div class="faq-list-section-item__question">Из чего состои уборка?</div>
                        <div class="faq-list-section-item__answer">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis hic incidunt molestias quia rem soluta voluptas. Aliquid cupiditate ex impedit, magnam mollitia nam natus neque, nesciunt numquam provident, repellat sapiente.</div>
                    </li>
                    <li class="faq-list-section-item">
                        <div class="faq-list-section-item__question">Из чего состои уборка?</div>
                        <div class="faq-list-section-item__answer">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis hic incidunt molestias quia rem soluta voluptas. Aliquid cupiditate ex impedit, magnam mollitia nam natus neque, nesciunt numquam provident, repellat sapiente.</div>
                    </li>
                </ul>
            </div>

        </div>
    </div>


</section>