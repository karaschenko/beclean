<?php
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
?>
<span id="service_prices" data-service-prices='<?= json_encode($prices); ?>'></span>
<section class="container-fluid section-first" data-parallax="scroll" data-image-src="<?=  Url::to('@web/img/section1.jpg')?>" >
    <div class="container">
        <div class="info-block row">
            <div class=" col-md-4 col-md-offset-2 col-xs-12 col-sm-8 col-sm-offset-2   info-block-text">
                <div class="info-block-text-main">
                    Уборка — не ваше дело
                </div>
                <div class="info-block-text-additional">
                    Сделайте заказ и занимайтесь своими делами, а уборку
                    мы возьмём на себя
                </div>
            </div>
            <div class="col-md-4 col-md-offset-0 col-xs-12 col-sm-8 col-sm-offset-2  col-sm-8  ">
                <div class="info-block-form" id="main-form">
                    <div class="info-block-form__title">Расскажите о квартире</div>
                    <div class="info-block-form-item">
                        <button class="info-block-form-item__btn info-block-form-item__btn_minus disabled" disabled  id="rooms_minus">-</button>
                        <div class="info-block-form-item__info ui_input">1-комнатная</div>
                        <button class="info-block-form-item__btn info-block-form-item__btn_plus" id="rooms_plus">+</button>
                    </div>
                    <div class="info-block-form-item">
                        <button class="info-block-form-item__btn info-block-form-item__btn_minus disabled" disabled id="bathroom_minus">-</button>
                        <div class="info-block-form-item__info ui_input">1-cанузел</div>
                        <button class="info-block-form-item__btn info-block-form-item__btn_plus " id="bathroom_plus">+</button>
                    </div>
                    <div class="info-block-form-item">
                        <div class="info-block-form-item__btn info-block-form-item__btn_tel material-icons">&#xE0B0;</div>
                        <input class="info-block-form-item__info ui_input" type="tel" pattern="\([0-9]{3}\)\s[0-9]{3}-[0-9]{2}-[0-9]{2}" name="phone" placeholder="(050) 121-34-57" title="Формат ввода (050) 121-34-57" required/>
                    </div>
                    <div class="info-block-form-item">
                        <a href="<?= Url::to(['site/order-form']); ?>" style="display: block;" class="info-block-form-item__info ui_btn" type="submit">Заказать от <span id="total_price">0</span> грн</a>
                    </div>
                </div>
                <input type="hidden" id="total_price_id" value="">
            </div>
        </div>
        <a href="#section2" class="next-section hidden-xs">Наши преимущества</a>
    </div>
</section>


<section class="container-fluid section-second" id="section2">
    <div class="container benefits-list">
        <div class="row hidden-xs hidden-sm">
            <div class="col-md-3 col-md-offset-0  col-xs-12 col-sm-8 col-sm-offset-2   benefits-list-item">
                <div class="benefits-list-item__image material-icons">&#xE422;</div>
                <div class="benefits-list-item__title">Удобное время</div>
                <div class="benefits-list-item__description">Наши клинеры приедут тогда, когда вы скажите. Например сегодня</div>
            </div>
            <div class="col-md-3 col-md-offset-0 col-xs-12 col-sm-8 col-sm-offset-2   benefits-list-item">
                <div class="benefits-list-item__image material-icons">&#xE545;</div>
                <div class="benefits-list-item__title">Экологическая уборка</div>
                <div class="benefits-list-item__description">Мы используем продукты из натурального сырья - они безопасны и гипоалергенны</div>
            </div>
            <div class="col-md-3 col-md-offset-0 col-xs-12 col-sm-8 col-sm-offset-2   benefits-list-item">
                <div class="benefits-list-item__image material-icons">&#xE8E8;</div>
                <div class="benefits-list-item__title">Надежные исполнители</div>
                <div class="benefits-list-item__description">У нас не бывает случайных людей - только те, что прошли жестокий отбор</div>
            </div>
            <div class="col-md-3 col-md-offset-0 col-xs-12 col-sm-8 col-sm-offset-2   benefits-list-item">
                <div class="benefits-list-item__image material-icons">&#xE8D0;</div>
                <div class="benefits-list-item__title">Честный подход</div>
                <div class="benefits-list-item__description">Гарантия возврата денег или бесплатного исправления работы</div>
            </div>
        </div>

        <div class="swiper-container visible-xs visible-sm">
            <!-- Additional required wrapper -->
            <div class="swiper-wrapper">
                <!-- Slides -->
                <div class="swiper-slide benefits-list-item">
                    <div class="benefits-list-item__image material-icons">&#xE422;</div>
                    <div class="benefits-list-item__title">Удобное время</div>
                    <div class="benefits-list-item__description">Наши клинеры приедут тогда, когда вы скажите. Например сегодня</div>
                </div>
                <div class="swiper-slide">
                    <div class="benefits-list-item__image material-icons">&#xE545;</div>
                    <div class="benefits-list-item__title">Экологическая уборка</div>
                    <div class="benefits-list-item__description">Мы используем продукты из натурального сырья - они безопасны и гипоалергенны</div>
                </div>
                <div class="swiper-slide">
                    <div class="benefits-list-item__image material-icons">&#xE8E8;</div>
                    <div class="benefits-list-item__title">Надежные исполнители</div>
                    <div class="benefits-list-item__description">У нас не бывает случайных людей - только те, что прошли жестокий отбор</div>
                </div>
                <div class="swiper-slide">
                    <div class="benefits-list-item__image material-icons">&#xE8D0;</div>
                    <div class="benefits-list-item__title">Честный подход</div>
                    <div class="benefits-list-item__description">Гарантия возврата денег или бесплатного исправления работы</div>
                </div>

            </div>


        </div>
    </div>

</section>


<section class="container-fluid section-third" data-parallax="scroll"  data-image-src="<?=  Url::to('@web/img/section-3.jpg')?>">
    <div class="container">
        <div class="row">
            <h1 class="col-md-6 col-md-offset-3 section-third__title">
                У нас все надежно!
            </h1>
        </div>
        <div class="row">

            <div class="col-md-3  col-sm-4   col-xs-10 col-xs-offset-1 col-md-offset-3">
                <div class="section-third-item">
                    <h2 class="section-third-item__title">
                        Проверяем исполнителей

                    </h2>
                    <div class="section-third-item__description">
                        Отбор клинеров проходит в несколькоэтапов — собеседование, тестирование,проверка службой безопасности. С намиостаются только лучшие из лучших
                    </div>
                </div>

                <div class="section-third-item">
                    <div class="section-third-item__title">
                        Принимаем ответственность

                    </div>
                    <div class="section-third-item__description">
                        Если что-то пошло не так, мы оперативнорешаем вопросы и исправляемнедоразумения
                    </div>
                </div>

                <div class="section-third-item">
                    <div class="section-third-item__title">
                        Защищаем платежи

                    </div>
                    <div class="section-third-item__description">
                        Мы не храним данные банковских карт, адля оплаты используем толькобезопасные каналы
                    </div>
                </div>

            </div>

            <div class="col-md-3 col-sm-4 col-xs-10 col-xs-offset-1 col-xs-12  ">
                <div class="section-third-item">
                    <div class="section-third-item__title">
                        Страхуем на 5 000 гривен
                    </div>

                    <div class="section-third-item__description">
                        Мы абсолютно уверены в своихисполнителях, но для вашего спокойствиястрахуем их ответственностьна 5 тысяч гривен
                    </div>

                </div>

                <div class="section-third-item">
                    <div class="section-third-item__title">
                        Следим за качеством
                    </div>

                    <div class="section-third-item__description">
                        Вы оцениваете работу уборщиков попятибалльной шкале, а мы следим, чтобыони всегда делали свою работуна отлично
                    </div>



                </div>
                <a href="<?= Url::to(['site/about']); ?>" class="section-third__btn ui_btn">Подробнее</a>

            </div>
        </div>
    </div>

</section>

<section class="section-fourth container-fluid">
    <div class="container">
        <h1 class="section-fourth__title ui_h1">
            Как это работает
        </h1>
        <div class="row hidden-xs hidden-sm">
            <div class="section-fourth-item col-md-4 col-md-offset-0 col-xs-12 col-sm-8 col-sm-offset-2  ">
                <div class="section-fourth-item__icon material-icons">&#xE400;</div>
                <div class="section-fourth-item__title">Закажите уборку</div>
                <div class="section-fourth-item__description">Расскажите где вы живете и что нужно сделать</div>

            </div>
            <div class="section-fourth-item col-md-4 col-md-offset-0 col-xs-12 col-sm-8 col-sm-offset-2  ">
                <div class="section-fourth-item__icon material-icons">&#xE401;</div>
                <div class="section-fourth-item__title">Дождитесь клинера</div>
                <div class="section-fourth-item__description">Или оставьте ключ, исполнитель прийдет со всем необходимым</div>
            </div>
            <div class="section-fourth-item col-md-4 col-md-offset-0 col-xs-12 col-sm-8 col-sm-offset-2  ">
                <div class="section-fourth-item__icon material-icons">&#xE3fb;</div>
                <div class="section-fourth-item__title">Платите за результат</div>
                <div class="section-fourth-item__description">И наслаждайтесь чистой квартирой</div>
            </div>
        </div>

        <!-- Slider main container -->
        <div class="swiper-container visible-xs visible-sm">
            <!-- Additional required wrapper -->
            <div class="swiper-wrapper">
                <!-- Slides -->
                <div class="swiper-slide section-fourth-item">
                    <div class="section-fourth-item__icon material-icons">&#xE400;</div>
                    <div class="section-fourth-item__title">Закажите уборку</div>
                    <div class="section-fourth-item__description">Расскажите где вы живете и что нужно сделать</div>
                </div>

                <div class="swiper-slide section-fourth-item">
                    <div class="section-fourth-item__icon material-icons">&#xE401;</div>
                    <div class="section-fourth-item__title">Дождитесь клинера</div>
                    <div class="section-fourth-item__description">Или оставьте ключ, исполнитель прийдет со всем необходимым</div>
                </div>

                <div class="swiper-slide section-fourth-item">
                    <div class="section-fourth-item__icon material-icons">&#xE3fb;</div>
                    <div class="section-fourth-item__title">Платите за результат</div>
                    <div class="section-fourth-item__description">И наслаждайтесь чистой квартирой</div>
                </div>



            </div>

        </div>


        <div class="col-md-4 col-xs-12 col-sm-8 col-sm-offset-2 col-md-offset-4">
            <a href="<?= Url::to(['site/faq']); ?>" class="section-fourth_btn ui_btn ui_btn_inverse ui_tac">Из чего состоит уборка</a>
        </div>


    </div>
</section>

<section class="section-fifth container-fluid" data-parallax="scroll"  data-image-src="<?=  Url::to('@web/img/section-5.jpg')?>">
    <div class="container">
        <h1 class="section-fifth__title">Что происходит прямо сейчас</h1>
        <div class="row hidden-xs hidden-sm">
            <div class="col-md-3 col-md-offset-0  col-xs-12 col-sm-8 col-sm-offset-2   section-fifth-item">
                <div class="section-fifth-item__number">1391</div>
                <div class="section-fifth-item__description">
                    квартир становится чище
                </div>
            </div>

            <div class="col-md-3 col-md-offset-0 col-xs-12 col-sm-8 col-sm-offset-2   section-fifth-item">
                <div class="section-fifth-item__number">19941</div>
                <div class="section-fifth-item__description">
                    чистая квартира на нашем счету
                </div>
            </div>
            <div class="col-md-3 col-md-offset-0 col-xs-12 col-sm-8 col-sm-offset-2   section-fifth-item">
                <div class="section-fifth-item__number">2542</div>
                <div class="section-fifth-item__description">
                    Клинера ждут вашего заказа
                </div>
            </div>
            <div class="col-md-3 col-md-offset-0 col-xs-12 col-sm-8 col-sm-offset-2   section-fifth-item">
                <div class="section-fifth-item__number">4.87</div>
                <div class="section-fifth-item__description">
                    Средрний рейтинг
                </div>
            </div>
        </div>

        <!-- Slider main container -->
        <div class="swiper-container visible-xs visible-sm">
            <!-- Additional required wrapper -->
            <div class="swiper-wrapper">
                <!-- Slides -->
                <div class="swiper-slide">
                    <div class="section-fifth-item__number">1391</div>
                    <div class="section-fifth-item__description">
                        квартир становится чище
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="section-fifth-item__number">19941</div>
                    <div class="section-fifth-item__description">
                        чистая квартира на нашем счету
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="section-fifth-item__number">2542</div>
                    <div class="section-fifth-item__description">
                        Клинера ждут вашего заказа
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="section-fifth-item__number">4.87</div>
                    <div class="section-fifth-item__description">
                        Средрний рейтинг
                    </div>
                </div>

            </div>

        </div>
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3   col-md-4 col-xs-12  col-md-offset-4">
                <a href="#main-form" class="section-fifth_btn ui_btn">Заказать уборку</a>
            </div>
        </div>
    </div>

</section>

<section class="section-sixth container-fluid">
    <div class="container">
        <h1 class="section-sixth__title ui_h1">
            Отзывы клиентов
        </h1>

        <div class="row hidden-xs hidden-sm">
            <div class="section-sixth-item">
                <div class="col-md-2 col-lg-2">
                    <div class="section-sixth-item__img" style="background-image: url('<?=  Url::to('@web/img/telo4ka.png')?>')"></div>
                </div>
                <div class="section-sixth-item-content col-md-4 col-xs-12 col-sm-12">
                    <div class="section-sixth-item-content__description">
                        «Последние несколько месяцев регулярно заказываю уборку на BeeClean. Приезжают вовремя, убирают чисто, а главное в случае каких либо проблем я всегда знаю куда обращаться. Теперь из моей душевой кабины видна вся остальная ванная комната, на батареях нет годового слоя пыли, а микроволновка изнутри почти стерильна. Заказывайте, они еще печеньки с предсказаниями оставляют на прощание»
                    </div>
                    <div class="section-sixth-item-content__author">
                        Мария Цымбал
                    </div>
                    <div class="section-sixth-item-content__company">
                        Melrose
                    </div>

                </div>
            </div>

            <div class="section-sixth-item">
                <div class="col-md-2 col-lg-2">
                    <div class="section-sixth-item__img" style="background-image: url('<?=  Url::to('@web/img/telo4ka.png')?>')"></div>
                </div>
                <div class="section-sixth-item-content col-md-4 col-xs-12 col-sm-12">
                    <div class="section-sixth-item-content__description">
                        «Последние несколько месяцев регулярно заказываю уборку на BeeClean. Приезжают вовремя, убирают чисто, а главное в случае каких либо проблем я всегда знаю куда обращаться. Теперь из моей душевой кабины видна вся остальная ванная комната, на батареях нет годового слоя пыли, а микроволновка изнутри почти стерильна. Заказывайте, они еще печеньки с предсказаниями оставляют на прощание»
                    </div>
                    <div class="section-sixth-item-content__author">
                        Мария Цымбал
                    </div>
                    <div class="section-sixth-item-content__company">
                        Melrose
                    </div>

                </div>
            </div>
        </div>
        <div class="row hidden-xs hidden-sm">
            <div class="section-sixth-item">
                <div class="col-md-2 col-lg-2">
                    <div class="section-sixth-item__img" style="background-image: url('<?=  Url::to('@web/img/telo4ka.png')?>')"></div>
                </div>
                <div class="section-sixth-item-content col-md-4 col-xs-12 col-sm-12">
                    <div class="section-sixth-item-content__description">
                        «Последние несколько месяцев регулярно заказываю уборку на BeeClean. Приезжают вовремя, убирают чисто, а главное в случае каких либо проблем я всегда знаю куда обращаться. Теперь из моей душевой кабины видна вся остальная ванная комната, на батареях нет годового слоя пыли, а микроволновка изнутри почти стерильна. Заказывайте, они еще печеньки с предсказаниями оставляют на прощание»
                    </div>
                    <div class="section-sixth-item-content__author">
                        Мария Цымбал
                    </div>
                    <div class="section-sixth-item-content__company">
                        Melrose
                    </div>

                </div>
            </div>

            <div class="section-sixth-item">
                <div class="col-md-2 col-lg-2">
                    <div class="section-sixth-item__img" style="background-image: url('<?=  Url::to('@web/img/telo4ka.png')?>')"></div>
                </div>
                <div class="section-sixth-item-content col-md-4 col-xs-12 col-sm-12">
                    <div class="section-sixth-item-content__description">
                        «Последние несколько месяцев регулярно заказываю уборку на BeeClean. Приезжают вовремя, убирают чисто, а главное в случае каких либо проблем я всегда знаю куда обращаться. Теперь из моей душевой кабины видна вся остальная ванная комната, на батареях нет годового слоя пыли, а микроволновка изнутри почти стерильна. Заказывайте, они еще печеньки с предсказаниями оставляют на прощание»
                    </div>
                    <div class="section-sixth-item-content__author">
                        Мария Цымбал
                    </div>
                    <div class="section-sixth-item-content__company">
                        Melrose
                    </div>

                </div>
            </div>
        </div>

        <!-- Slider main container -->
        <div class="swiper-container visible-xs visible-sm">
            <!-- Additional required wrapper -->
            <div class="swiper-wrapper">
                <!-- Slides -->
                <div class="swiper-slide">
                    <div class="section-sixth-item__img" style="background-image: url('<?=  Url::to('@web/img/telo4ka.png')?>')"></div>

                    <div class="section-sixth-item-content col-md-4 col-xs-12 col-sm-12">
                        <div class="section-sixth-item-content__description">
                            «Последние несколько месяцев регулярно заказываю уборку на BeeClean. Приезжают вовремя, убирают чисто, а главное в случае каких либо проблем я всегда знаю куда обращаться. Теперь из моей душевой кабины видна вся остальная ванная комната, на батареях нет годового слоя пыли, а микроволновка изнутри почти стерильна. Заказывайте, они еще печеньки с предсказаниями оставляют на прощание»
                        </div>
                        <div class="section-sixth-item-content__author">
                            Мария Цымбал
                        </div>
                        <div class="section-sixth-item-content__company">
                            Melrose
                        </div>

                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="section-sixth-item__img" style="background-image: url('<?=  Url::to('@web/img/telo4ka.png')?>')"></div>

                    <div class="section-sixth-item-content col-md-4 col-xs-12 col-sm-12">
                        <div class="section-sixth-item-content__description">
                            «Последние несколько месяцев регулярно заказываю уборку на BeeClean. Приезжают вовремя, убирают чисто, а главное в случае каких либо проблем я всегда знаю куда обращаться. Теперь из моей душевой кабины видна вся остальная ванная комната, на батареях нет годового слоя пыли, а микроволновка изнутри почти стерильна. Заказывайте, они еще печеньки с предсказаниями оставляют на прощание»
                        </div>
                        <div class="section-sixth-item-content__author">
                            Мария Цымбал
                        </div>
                        <div class="section-sixth-item-content__company">
                            Melrose
                        </div>

                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="section-sixth-item__img" style="background-image: url('<?=  Url::to('@web/img/telo4ka.png')?>')"></div>

                    <div class="section-sixth-item-content col-md-4 col-xs-12 col-sm-12">
                        <div class="section-sixth-item-content__description">
                            «Последние несколько месяцев регулярно заказываю уборку на BeeClean. Приезжают вовремя, убирают чисто, а главное в случае каких либо проблем я всегда знаю куда обращаться. Теперь из моей душевой кабины видна вся остальная ванная комната, на батареях нет годового слоя пыли, а микроволновка изнутри почти стерильна. Заказывайте, они еще печеньки с предсказаниями оставляют на прощание»
                        </div>
                        <div class="section-sixth-item-content__author">
                            Мария Цымбал
                        </div>
                        <div class="section-sixth-item-content__company">
                            Melrose
                        </div>

                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="section-sixth-item__img" style="background-image: url('<?=  Url::to('@web/img/telo4ka.png')?>')"></div>

                    <div class="section-sixth-item-content col-md-4 col-xs-12 col-sm-12">
                        <div class="section-sixth-item-content__description">
                            «Последние несколько месяцев регулярно заказываю уборку на BeeClean. Приезжают вовремя, убирают чисто, а главное в случае каких либо проблем я всегда знаю куда обращаться. Теперь из моей душевой кабины видна вся остальная ванная комната, на батареях нет годового слоя пыли, а микроволновка изнутри почти стерильна. Заказывайте, они еще печеньки с предсказаниями оставляют на прощание»
                        </div>
                        <div class="section-sixth-item-content__author">
                            Мария Цымбал
                        </div>
                        <div class="section-sixth-item-content__company">
                            Melrose
                        </div>

                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="section-sixth-item__img" style="background-image: url('<?=  Url::to('@web/img/telo4ka.png')?>')"></div>

                    <div class="section-sixth-item-content col-md-4 col-xs-12 col-sm-12">
                        <div class="section-sixth-item-content__description">
                            «Последние несколько месяцев регулярно заказываю уборку на BeeClean. Приезжают вовремя, убирают чисто, а главное в случае каких либо проблем я всегда знаю куда обращаться. Теперь из моей душевой кабины видна вся остальная ванная комната, на батареях нет годового слоя пыли, а микроволновка изнутри почти стерильна. Заказывайте, они еще печеньки с предсказаниями оставляют на прощание»
                        </div>
                        <div class="section-sixth-item-content__author">
                            Мария Цымбал
                        </div>
                        <div class="section-sixth-item-content__company">
                            Melrose
                        </div>

                    </div>
                </div>



            </div>

        </div>


        <!--<div class="row">-->
            <!--<div class="col-md-4 col-xs-12 col-sm-8 col-sm-offset-2   col-md-offset-4 ui_tac">-->
                <!--<button class="section-sixth_btn ui_btn ui_btn_inverse">Показать больше отзывов</button>-->
            <!--</div>-->
        <!--</div>-->
    </div>


</section>




