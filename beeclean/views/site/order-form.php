<?php
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
?>
<span id="service_prices" data-service-prices='<?= json_encode($prices); ?>'></span>
<?php ActiveForm::begin(['action' => ['site/order']]); ?>
<section class="container-fluid section-first" data-parallax="scroll" data-image-src="<?=  Url::to('@web/img/order-form1.jpg')?>" >
    <div class="container">
        <div class="info-block row">

            <div class="col-md-6 col-md-offset-3 col-xs-12 col-sm-8 col-sm-offset-2  col-sm-8">
                <div class="info-block-form">
                    <div class="info-block-form__title">Заказ уборки</div>
                    <div class="info-block-form-item">
                        <div class="info-block-form-item__btn info-block-form-item__btn_minus material-icons">&#xE84E;</div>
                        <input class="info-block-form-item__info ui_input tal" type="text"  placeholder="Фамилия" name="Order[last_name]" required/>
                    </div>
                    <div class="info-block-form-item">
                        <div class="info-block-form-item__btn info-block-form-item__btn_minus material-icons">&#xE84E;</div>
                        <input class="info-block-form-item__info ui_input tal" type="text" n placeholder="Имя" name="Order[first_name]" required/>
                    </div>
                    <div class="info-block-form-item">
                        <div class="info-block-form-item__btn info-block-form-item__btn_minus material-icons">&#xE84E;</div>
                        <input class="info-block-form-item__info ui_input tal" type="text"  placeholder="Отчество" name="Order[middle_name]" required/>
                    </div>

                    <div class="info-block-form-item">
                        <div class="info-block-form-item__btn info-block-form-item__btn_tel material-icons">&#xE0B0;</div>
                        <input class="info-block-form-item__info ui_input tal" type="tel" pattern="\([0-9]{3}\)\s[0-9]{3}-[0-9]{2}-[0-9]{2}" name="Address[phone]" placeholder="(050) 121-34-57" title="Формат ввода (050) 121-34-57" required/>
                    </div>
                    <div class="info-block-form-item">
                        <div class="info-block-form-item__btn info-block-form-item__btn_minus material-icons">&#xE0BE;</div>
                        <input class="info-block-form-item__info ui_input tal" type="email"  placeholder="E-mail" name="Address[email]" required/>
                    </div>

                    <div class="info-block-form-item">
                        <div class="info-block-form-item__btn info-block-form-item__btn_minus material-icons">&#xE916;</div>
                        <input class="info-block-form-item__info ui_input tal" type="text" id="basic_example_1" name="Order[date]"  placeholder="Дата уборки" required/>
                    </div>






                </div>

            </div>
        </div>
        <a href="#section2" class="next-section hidden-xs">Следующий этап</a>
    </div>
</section>


<section class="container-fluid section-second" id="section2">
    <div class="container">
        <div class="info-block dark row">
            <div class="col-md-6 col-md-offset-0 col-xs-12 col-sm-8 col-sm-offset-2  col-sm-8">
                <div class="info-block-form">
                    <div class="info-block-form__title">Расскажите где нужно убрать</div>
                    <div class="info-block-form-item">
                        <div class="info-block-form-item__btn info-block-form-item__btn_minus material-icons">&#xE922;</div>
                        <input class="info-block-form-item__info ui_input tal" type="text" name="Address[street]" placeholder="Улица"  required id="order_street"/>
                    </div>
                    <div class="info-block-form-item">
                        <div class="info-block-form-item__btn info-block-form-item__btn_minus material-icons">&#xE88A;</div>
                        <input class="info-block-form-item__info ui_input tal" type="text" name="Address[house]" placeholder="Дом" required id="order_street_number"/>
                    </div>
                    <div class="info-block-form-item">
                        <div class="info-block-form-item__btn info-block-form-item__btn_minus material-icons">&#xE630;</div>
                        <input class="info-block-form-item__info ui_input tal" type="text" name="Address[apartment]" placeholder="Квартира" required/>
                    </div>


                    <div class="info-block-form-item">
                        <div class="info-block-form-item__btn info-block-form-item__btn_minus material-icons">&#xE567;</div>
                        <input class="info-block-form-item__info ui_input tal" type="text" name="Address[city]" value="Днепр" required/>
                    </div>

                    <div class="info-block-form-item">
                        <div class="info-block-form-item__btn info-block-form-item__btn_minus material-icons">&#xE567;</div>
                        <input class="info-block-form-item__info ui_input tal" type="text" name="Address[country]" value="Украина" required/>
                    </div>

                    <div class="info-block-form-item textarea">

                        <textarea class="info-block-form-item__info ui_input tal" name="Order[editional_info]" row="3" id="" placeholder="Дополнительная информация"></textarea>
                    </div>
                </div>


            </div>
            <div class="col-md-6 col-md-offset-0 col-xs-12 col-sm-8 col-sm-offset-2  col-sm-8">
                <h2 class="ui_h2 without-lh">Укажите на карте</h2>
                <div id="order_map"></div>
            </div>
        </div>
        <a href="#section3" class="next-section hidden-xs inverse">Следующий этап</a>
    </div>

</section>


<section class="container-fluid section-third" id="section3" data-parallax="scroll"  data-image-src="<?=  Url::to('@web/img/order-form2.jpg')?>">
    <div class="container">
        <h1 class="ui_h1 white">Фронт работ</h1>
        <div class="info-block small-margin row">


            <div class="col-md-6 col-md-offset-3 col-xs-12 col-sm-8 col-sm-offset-2  col-sm-8">
                <div class="info-block-form">
                    <div class="info-block-form__title">Сколько у вас квадратных метров?</div>
                    <div class="info-block-form-item range">
                        <div class="info-block-form-item__range">
                            <input class="info-block-form-item__range" name="Address[square]" id="square" type="rang" />
                        </div>
                        <div id="js-display-square" class="info-block-form-item__range_value">0</div>

                    </div>

                </div>

            </div>
        </div>
        <div class="info-block small-margin row">
            <div class="col-md-6 col-md-offset-3 col-xs-12 col-sm-8 col-sm-offset-2  col-sm-8">
                <div class="info-block-form ">
                    <div class="info-block-form__title">А комнат?</div>
                    <div class="info-block-form-item range">
                        <div class="info-block-form-item__range">
                            <input class="info-block-form-item__range" id="irooms" name="Address[rooms]" type="rang" />
                        </div>
                        <div id="js-display-irooms" class="info-block-form-item__range_value">1</div>

                    </div>
                </div>

            </div>
        </div>
        <div class="info-block small-margin row">
            <div class="col-md-6 col-md-offset-3 col-xs-12 col-sm-8 col-sm-offset-2  col-sm-8">
                <div class="info-block-form ">
                    <div class="info-block-form__title">А санузлов?</div>
                    <div class="info-block-form-item range">
                        <div class="info-block-form-item__range">
                            <input class="info-block-form-item__range" name="Address[bathrooms]" id="ibathrooms" type="rang" />
                        </div>
                        <div id="js-display-ibathrooms" class="info-block-form-item__range_value">1</div>

                    </div>
                </div>

            </div>
        </div>

    </div>
    <a href="#section4" class="next-section hidden-xs">Следующий этап</a>

</section>

<section class="section-fourth container-fluid" id="section4">
    <div class="container">

        <div class="info-block dark row">

            <div class="col-md-6 col-md-offset-3 col-xs-12 col-sm-8 col-sm-offset-2  col-sm-8">
                <div class="info-block-form ">
                    <div class="info-block-form__title">Дополнительные услуги</div>

                    <?php foreach($services as $service): ?>
                        <div class="info-block-form-item">
                            <label class="info-block-form-item__btn info-block-form-item__btn_minus material-icons">
                                <input type="checkbox" name="Order[services][<?= $service['id']; ?>]" class="info-block-form-item__checkbox" data-other-price='{"id": "<?= $service['id']; ?>", "price": "<?= $service['price']; ?>"}' >
                            </label>

                            <div type="text" class="info-block-form-item__info ui_input tal"><?= $service['name']; ?> </div>
                        </div>
                    <?php endforeach; ?>
                </div>

            </div>
        </div>



    </div>
</section>

<section class="section-fourth container-fluid" id="section5"  style="min-height: 560px" data-parallax="scroll"  data-image-src="<?=  Url::to('@web/img/section-5.jpg')?>">
    <div class="container">

        <div class="info-block  row">

            <div class="col-md-6 col-md-offset-3 col-xs-12 col-sm-8 col-sm-offset-2  col-sm-8">
                <div class="info-block-form ">
                    <div class="info-block-form__title">Регулярная уборка</div>

                    <div class="info-block-form-item">
                        <label class="info-block-form-item__btn info-block-form-item__btn_minus material-icons">
                            <input type="checkbox" name="Order[is_regular]" id="subscribe" class="info-block-form-item__checkbox">
                        </label>

                        <div type="text" class="info-block-form-item__info ui_input tal">Оформить подписку</div>

                    </div>
                    <div class="info-block-form-item textarea" id="subscribe-area" >

                        <textarea class="info-block-form-item__info ui_input tal" name="Order[regular_description]" row="3" placeholder="Укажите ваши условия"></textarea>
                    </div>

                </div>

            </div>
        </div>



    </div>
</section>

<section class="container-fluid">
    <div class="container">

        <div class="info-block dark row">

            <div class="col-md-6 col-md-offset-3 col-xs-12 col-sm-8 col-sm-offset-2  col-sm-8">
                <div class="info-block-form ">
                    <div class="info-block-form-item">
                        <div class="info-block-form-item__btn info-block-form-item__btn_minus material-icons">&#xE227;</div>
                        <div class="info-block-form-item__info ui_input">Итоговая стоимость: <span id="totalPrice"></span>  грн</div>
                    </div>

                    <div class="info-block-form-item ">
                        <button class="info-block-form-item__info ui_btn" type="submit">Оплатить через LiqPay</button>
                    </div>
                    <div class="info-block-form-item ">
                        <button class="info-block-form-item__info ui_btn" type="submit">Оплатить через Оператора</button>
                    </div>


                </div>

            </div>
        </div>



    </div>
</section>
<?php ActiveForm::end(); ?>
<!--<div class="modal-layout" id="order">-->
    <!--<div class="modal-layout__close"></div>-->
    <!--<div class="modal-window">-->
        <!--<div class="material-icons modal-close">&#xE5C9;</div>-->

        <!--<div class="info-block-form ">-->
            <!--<h2 class="ui_h2">Заказ №2222222</h2>-->

            <!--<div class="info-block-form-item">-->
                <!--<label class="info-block-form-item__btn info-block-form-item__btn_minus material-icons">-->
                    <!--<input type="checkbox" class="info-block-form-item__checkbox">-->
                <!--</label>-->

                <!--<div type="text" class="info-block-form-item__info ui_input tal">Дополнительная услуга 1 </div>-->
            <!--</div>-->
            <!--<div class="info-block-form-item">-->
                <!--<label class="info-block-form-item__btn info-block-form-item__btn_minus material-icons">-->
                    <!--<input type="checkbox" class="info-block-form-item__checkbox">-->
                <!--</label>-->

                <!--<div type="text" class="info-block-form-item__info ui_input tal">Дополнительная услуга 2 </div>-->
            <!--</div>-->

            <!--<div class="info-block-form-item">-->
                <!--<button class="info-block-form-item__info ui_btn" type="submit">Оплатить через LiqPay</button>-->
            <!--</div>-->
            <!--<div class="info-block-form-item">-->
                <!--<button class="info-block-form-item__info ui_btn" type="submit">Оплатить через Оператора</button>-->
            <!--</div>-->
        <!--</div>-->
    <!--</div>-->
<!--</div>-->


<script>
    var service_prices = JSON.parse(document.querySelectorAll('#service_prices')[0].getAttribute('data-service-prices'));
    var mainPagePriceId = window.localStorage.getItem('mainPagePriceId');

    var otherPriceViews = document.querySelectorAll('*[data-other-price]');

    var totalPriceComp = {
        rooms: 0,
        other: {}
    };

    var otherPrice = {};
    for (var i = 0; i < otherPriceViews.length; i++) {
        otherPrice[JSON.parse(otherPriceViews[i].getAttribute('data-other-price')).id] = JSON.parse(otherPriceViews[i].getAttribute('data-other-price')).price;
        totalPriceComp.other[JSON.parse(otherPriceViews[i].getAttribute('data-other-price')).id] = false;

        otherPriceViews[i].onchange = function(){
            totalPriceComp.other[JSON.parse(this.getAttribute('data-other-price')).id] = this.checked;
            calcTotalPrice();
        };
    }

    calcTotalPrice();



    function calcTotalPrice(){
        var price = parseInt(totalPriceComp.rooms);
        for (var key in totalPriceComp.other) {
            if (totalPriceComp.other[key]) {
                price += parseInt(otherPrice[key]);
            }
        }

        document.querySelector('#totalPrice').innerHTML = String(price);
        //console.log(String(price));
    }



    var defaultPrices = {
        rooms: 1,
        bathrooms: 1
    };

    var square = document.querySelector('#square');
    var initSquare = new Powerange(square,{ min: 0, max: 50, start: 0 });

    var changeSquare = document.querySelector('#square');

    changeSquare.onchange = function() {
        document.getElementById('js-display-square').innerHTML = changeSquare.value;
    };


    var irooms = document.querySelector('#irooms');
    var initIrooms = new Powerange(irooms,{ min: 1, max: 5, start: 1 });

    var changeIrooms = document.querySelector('#irooms');

    changeIrooms.onchange = function() {
        defaultPrices.rooms = this.value;
        setNewPrices();
        document.getElementById('js-display-irooms').innerHTML = changeIrooms.value;
    };



    var ibathrooms = document.querySelector('#ibathrooms');
    var initIbathrooms = new Powerange(ibathrooms,{ min: 1, max: 5, start: 1 });

    var changeIbathrooms = document.querySelector('#ibathrooms');

    changeIbathrooms.onchange = function() {
        defaultPrices.bathrooms = this.value;
        setNewPrices();
        document.getElementById('js-display-ibathrooms').innerHTML = changeIbathrooms.value;
    };

    //****************************************************



    for (var i = 0; i < service_prices.length; i++) {
        if (service_prices[i].id == mainPagePriceId) {
            resetControls(service_prices[i]);
            totalPriceComp.rooms = service_prices[i].price;
            calcTotalPrice();
            break;
        }
    }

    function resetControls(prices) {

        initIrooms.setStart(prices.rooms);
        initIbathrooms.setStart(prices.bathrooms);

        defaultPrices = {
            rooms: parseInt(prices.rooms),
            bathrooms: parseInt(prices.bathrooms)
        };
    }

    function setNewPrices(){
        for (i = 0; i < service_prices.length; i++) {
            if (service_prices[i].bathrooms == defaultPrices.bathrooms && service_prices[i].rooms == defaultPrices.rooms) {
                window.localStorage.setItem('mainPagePriceId', service_prices[i].id);
                totalPriceComp.rooms = service_prices[i].price;
                calcTotalPrice();
                break;
            }
        }
    }
</script>