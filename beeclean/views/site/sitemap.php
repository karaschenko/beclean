
<section class="sitemap container-fluid">
    <div class="container">
        <h1 class="sitemap__title ui_h1">
            Карта сайта
        </h1>
        <div class="row sitemap-list">
            <div class="col-md-4 col-sm-6 col-xs-12">
                <h2 class="sitemap-list__title ui_h2">
                    Полезная информация
                </h2>
                <ul class="sitemap-list-section">
                    <li class="sitemap-list__item"><a href="about.html">О сервисе</a></li>
                    <li class="sitemap-list__item"><a href="faq.html">Вопросы и ответы</a></li>
                    <li class="sitemap-list__item"><a href="#">Приведи друга</a></li>
                    <li class="sitemap-list__item"><a href="blog.html">Блог</a></li>
                    <li class="sitemap-list__item"><a href="#">Вакансии</a></li>
                    <li class="sitemap-list__item"><a href="becleaner.html">Работа в BeeClean</a></li>

                </ul>

                <h2 class="sitemap-list__title ui_h2">
                    Полезная информация
                </h2>
                <ul class="sitemap-list-section">
                    <li class="sitemap-list__item"><a href="about.html">О сервисе</a></li>
                    <li class="sitemap-list__item"><a href="faq.html">Вопросы и ответы</a></li>
                    <li class="sitemap-list__item"><a href="#">Приведи друга</a></li>
                    <li class="sitemap-list__item"><a href="blog.html">Блог</a></li>
                    <li class="sitemap-list__item"><a href="#">Вакансии</a></li>
                    <li class="sitemap-list__item"><a href="becleaner.html">Работа в BeeClean</a></li>

                </ul>

            </div>

            <div class="col-md-4 col-sm-6 col-xs-12">
                <h2 class="sitemap-list__title ui_h2">
                    Полезная информация
                </h2>
                <ul class="sitemap-list-section">
                    <li class="sitemap-list__item"><a href="about.html">О сервисе</a></li>
                    <li class="sitemap-list__item"><a href="faq.html">Вопросы и ответы</a></li>
                    <li class="sitemap-list__item"><a href="#">Приведи друга</a></li>
                    <li class="sitemap-list__item"><a href="blog.html">Блог</a></li>
                    <li class="sitemap-list__item"><a href="#">Вакансии</a></li>
                    <li class="sitemap-list__item"><a href="becleaner.html">Работа в BeeClean</a></li>

                </ul>

                <h2 class="sitemap-list__title ui_h2">
                    Полезная информация
                </h2>
                <ul class="sitemap-list-section">
                    <li class="sitemap-list__item"><a href="about.html">О сервисе</a></li>
                    <li class="sitemap-list__item"><a href="faq.html">Вопросы и ответы</a></li>
                    <li class="sitemap-list__item"><a href="#">Приведи друга</a></li>
                    <li class="sitemap-list__item"><a href="blog.html">Блог</a></li>
                    <li class="sitemap-list__item"><a href="#">Вакансии</a></li>
                    <li class="sitemap-list__item"><a href="becleaner.html">Работа в BeeClean</a></li>

                </ul>

            </div>

            <div class="col-md-4 col-sm-6 col-xs-12">
                <h2 class="sitemap-list__title ui_h2">
                    Полезная информация
                </h2>
                <ul class="sitemap-list-section">
                    <li class="sitemap-list__item"><a href="about.html">О сервисе</a></li>
                    <li class="sitemap-list__item"><a href="faq.html">Вопросы и ответы</a></li>
                    <li class="sitemap-list__item"><a href="#">Приведи друга</a></li>
                    <li class="sitemap-list__item"><a href="blog.html">Блог</a></li>
                    <li class="sitemap-list__item"><a href="#">Вакансии</a></li>
                    <li class="sitemap-list__item"><a href="becleaner.html">Работа в BeeClean</a></li>

                </ul>

                <h2 class="sitemap-list__title ui_h2">
                    Полезная информация
                </h2>
                <ul class="sitemap-list-section">
                    <li class="sitemap-list__item"><a href="about.html">О сервисе</a></li>
                    <li class="sitemap-list__item"><a href="faq.html">Вопросы и ответы</a></li>
                    <li class="sitemap-list__item"><a href="#">Приведи друга</a></li>
                    <li class="sitemap-list__item"><a href="blog.html">Блог</a></li>
                    <li class="sitemap-list__item"><a href="#">Вакансии</a></li>
                    <li class="sitemap-list__item"><a href="becleaner.html">Работа в BeeClean</a></li>

                </ul>

            </div>

        </div>
    </div>


</section>