window.onload = function(){
    googleMap.init({
        cbName: 'initMap',
        mapContainer: document.querySelectorAll('.container-fluid.map')[0]
    });

    googleMap.init({
        cbName: 'initOrderMap',
        mapContainer: document.querySelectorAll('#order_map')[0]
    });
};


var styles = [
    {
        "featureType": "administrative",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "administrative.country",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "administrative.country",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "administrative.locality",
        "elementType": "geometry",
        "stylers": [
            {
                "visibility": "off"
            },
            {
                "color": "#d6bc68"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#e7cd79"
            },
            {
                "weight": 0.1
            }
        ]
    },
    {
        "featureType": "landscape.natural.landcover",
        "elementType": "geometry",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#d6bc68"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#d6bc68"
            }
        ]
    },
    {
        "featureType": "poi.attraction",
        "elementType": "geometry",
        "stylers": [
            {
                "visibility": "off"
            },
            {
                "color": "#cfb665"
            }
        ]
    },
    {
        "featureType": "poi.business",
        "elementType": "all",
        "stylers": [
            {
                "color": "#e9d9a6"
            },
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "poi.government",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi.medical",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi.place_of_worship",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi.school",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi.sports_complex",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "geometry",
        "stylers": [
            {
                "visibility": "simplified"
            },
            {
                "color": "#e9d9a6"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "visibility": "off"
            },
            {
                "weight": 1
            },
            {
                "color": "#e9d9a6"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry",
        "stylers": [
            {
                "visibility": "simplified"
            },
            {
                "color": "#e9d9a6"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "labels.text",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road.highway.controlled_access",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#d6bc68"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "color": "#cfb665"
            },
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit.line",
        "elementType": "all",
        "stylers": [
            {
                "color": "#d6bc68"
            },
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "transit.station.airport",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "visibility": "off"
            },
            {
                "color": "#d6bc68"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "simplified"
            },
            {
                "color": "#282828"
            }
        ]
    }
];

var POSITION = {lat: 48.46329, lng: 35.049002},
    MARKER = '../img/map_pin.png';


var googleMap = {
    key: 'AIzaSyDT_x_FUDz4zSMR0_6xLxUL8rtqU2SJv2k',
    scriptElement: null,
    url: function(key, callback){
        return 'https://maps.googleapis.com/maps/api/js?key='+key+'&callback='+callback;
    },
    init: function(data){

        if (data.mapContainer != undefined) {
            this.scriptElement = document.createElement('script');
            this.scriptElement.setAttribute('src', this.url(this.key, data.cbName));
            this.scriptElement.setAttribute('async', null);
            this.scriptElement.setAttribute('defer', null);

            document.head.appendChild(this.scriptElement);
        }
    }
};

function initMap() {
    document.head.removeChild(googleMap.scriptElement);

    var map = new google.maps.Map(document.querySelectorAll('.container-fluid.map')[0], {
        navigationControl:  false,
        mapTypeControl:     false,
        scrollwheel:        false,
        center:             POSITION,
        styles:             styles,
        mapTypeId:          google.maps.MapTypeId.ROADMAP,
        zoom:               13
    });

    var markerImage = new google.maps.MarkerImage(
        MARKER,
        new google.maps.Size(56,76),
        new google.maps.Point(0,0),
        new google.maps.Point(28,76)
    );

    var marker = new google.maps.Marker({
        icon: markerImage,
        position: POSITION,
        draggable: false,
        map: map
    });
}

function initOrderMap() {
    var markers = [];
    var markerImage = new google.maps.MarkerImage(
        MARKER,
        new google.maps.Size(56,76),
        new google.maps.Point(0,0),
        new google.maps.Point(28,76)
    );

    document.head.removeChild(googleMap.scriptElement);

    var map = new google.maps.Map(document.querySelectorAll('#order_map')[0], {
        navigationControl:  false,
        mapTypeControl:     false,
        scrollwheel:        true,
        center:             POSITION,
        styles:             styles,
        mapTypeId:          google.maps.MapTypeId.ROADMAP,
        zoom:               13
    });

    map.addListener('click', function(e) {
        var geo = e.latLng;
        placeMarkerAndPanTo(geo, map);

        geo = String(geo);
        geo = geo.substr(1, geo.length-2).split(/, /);


        $.ajax({
            url: 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' + geo[0] + ',' + geo[1],
            success: function(e){
                if (e.status == 'OK') {
                    var response = e.results[0].address_components;
                    var results = new Array();

                    for (var i = 0; i < response.length; i++) {
                        results[response[i].types[0]] = response[i].long_name;
                        if (i == response.length - 1) {
                            setAdress(results);
                        }
                    }

                } else {
                    console.log(e);
                }

            }
        });
    });

    function placeMarkerAndPanTo(latLng, map) {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(null);
        }

        markers = [];


        markers.push(new google.maps.Marker({
            icon: markerImage,
            position: latLng,
            map: map
        }));


        map.panTo(latLng);
    }


    function setAdress(data){
        console.log(data);

        document.getElementById('order_street').value = data.route;
        document.getElementById('order_street_number').value = data.street_number;
    }
}
