
$(document).ready(function () {

    var pricesView = $('#service_prices')[0];
    if (pricesView) {
        var prices = JSON.parse(pricesView.getAttribute('data-service-prices'));
        if (prices.length >=1 ) {
            var test_arr = {
                maxBathrooms: prices[0].bathrooms,
                maxRooms: prices[0].rooms
            };

            for (var i = 0; i < prices.length; i++) {
                if (prices[i].bathrooms > test_arr.maxBathrooms) test_arr.maxBathrooms = prices[i].bathrooms;
                if (prices[i].rooms > test_arr.maxRooms) test_arr.maxRooms = prices[i].rooms;

                if (i == prices.length-1 ) initCalculation(test_arr);
            }
        }
    }

    function initCalculation(maxRoomsValues) {
        var roomAdd = $('#rooms_plus')[0],
            roomRemove = $('#rooms_minus')[0],
            roomInput = $('#rooms_plus').siblings('.info-block-form-item__info')[0],
            roomCounter = 0,
            bathroomAdd = $('#bathroom_plus')[0],
            bathroomRemove = $('#bathroom_minus')[0],
            bathroomCounter = 0,
            bathroomInput = $('#bathroom_plus').siblings('.info-block-form-item__info')[0],
            totalPrice = $('#total_price')[0],
            totalPriceInput = $("#total_price_id")[0],
            consts = {
                rooms: ['1-комнатная', '2-комнатная', '3-комнатная', '4-комнатная', '5-комнатная'],
                bathrooms: ['1 санузел', '2 санузла', '3 санузла', '4 санузла', '5 санузлов']
            };


        if (roomAdd && roomRemove && roomInput) {
            roomInput.innerHTML = consts.rooms[roomCounter];
            bathroomInput.innerHTML = consts.bathrooms[bathroomCounter];
            checkRoomsMaxValue();
            checkBathroomsMaxValue();
            calculateTotalPrice(roomCounter, bathroomCounter);

            roomAdd.onclick = function(){
                roomCounter++;
                roomInput.innerHTML = consts.rooms[roomCounter];
                checkRoomsMaxValue();
                calculateTotalPrice(roomCounter, bathroomCounter);
            };
            roomRemove.onclick = function(){
                roomCounter--;
                roomInput.innerHTML = consts.rooms[roomCounter];
                checkRoomsMaxValue();
                calculateTotalPrice(roomCounter, bathroomCounter);
            };
            bathroomAdd.onclick = function(){
                bathroomCounter++;
                bathroomInput.innerHTML = consts.bathrooms[bathroomCounter];
                checkBathroomsMaxValue();
                calculateTotalPrice(roomCounter, bathroomCounter);
            };
            bathroomRemove.onclick = function(){
                bathroomCounter--;
                bathroomInput.innerHTML = consts.bathrooms[bathroomCounter];
                checkBathroomsMaxValue();
                calculateTotalPrice(roomCounter, bathroomCounter);
            };
        }


        function checkRoomsMaxValue(){
            if (roomCounter > 0) {
                roomRemove.removeAttribute('disabled');
                roomRemove.classList.remove('disabled');
            } else {
                roomRemove.setAttribute('disabled', 'true');
                roomRemove.classList.add('disabled');
            }

            if (roomCounter < maxRoomsValues.maxRooms-1) {
                roomAdd.removeAttribute('disabled');
                roomAdd.classList.remove('disabled');
            } else {
                roomAdd.setAttribute('disabled', 'true');
                roomAdd.classList.add('disabled');
            }
        }
        function checkBathroomsMaxValue(){
            if (bathroomCounter > 0) {
                bathroomRemove.removeAttribute('disabled');
                bathroomRemove.classList.remove('disabled');
            } else {
                bathroomRemove.setAttribute('disabled', 'true');
                bathroomRemove.classList.add('disabled');
            }

            if (bathroomCounter < maxRoomsValues.maxBathrooms-1) {
                bathroomAdd.removeAttribute('disabled');
                bathroomAdd.classList.remove('disabled');
            } else {
                bathroomAdd.setAttribute('disabled', 'true');
                bathroomAdd.classList.add('disabled');
            }
        }
        function calculateTotalPrice(rooms, bathrooms) {
            for (i = 0; i < prices.length; i++) {
                if (prices[i].bathrooms == bathrooms+1 && prices[i].rooms == rooms+1) {
                    totalPrice.innerHTML = prices[i].price;
                    totalPriceInput.value = prices[i].id;
                    window.localStorage.setItem('mainPagePriceId', prices[i].id);
                }
            }
        }
    }



    //***************** контроль модалок для адресов ****************************
    var addresses = document.querySelectorAll('*[data-adress-modal]');

    if (typeof addresses == 'object') {
        for (var i = 0; i < addresses.length; i++) {
            addresses[i].onclick = function(){
                $('#single_adres').css("display","flex");
                refreshAddressModalData(this.getAttribute('data-adress-modal'));
            }
        }
    }

    function refreshAddressModalData(id) {
        console.log(id);
    }
    //***************************************************************************



    $('.faq-list-section-item__question').on('click', function() {
        $(this).siblings('.faq-list-section-item__answer').toggle();
        $(this).toggleClass('active');


    });


    $(".tabs-control__item").on('click', function (e) {
        e.preventDefault();
        var item = $(this);
        var currentItem = $(".tabs-list__item");
        var itemPosition = item.index();
        item.addClass('primary').siblings().removeClass('primary');
        currentItem.eq(itemPosition).add(item).addClass('active').siblings().removeClass('active');
    });

    $('#dashboard-change').on('click', function() {
       $("#change-data").css("display","flex");

     });
    $('#login_btn').on('click', function(e) {
        e.preventDefault();
        $("#login").css("display","flex");

    });
    $('#order-open').on('click', function(e) {
        e.preventDefault();
        $("#order").css("display","flex");

    });

    $('#password-change').on('click', function() {
        $("#password-data").css("display","flex");

    });
    $('#add-address').on('click', function() {
        $("#new-address").css("display","flex");

    });

    $('.modal-close').on('click', function() {
        $(".modal-layout").css("display","none");

    });
    $('.modal-layout__close').on('click', function() {
        $(".modal-layout").css("display","none");

    });

    $(window).scroll(function() {
        if($(window).scrollTop() >= 100) {

            $(".header").addClass('scrolled');
        }
        else if($(window).scrollTop() <= 100) {
            $(".header").removeClass('scrolled');
        }
    });

    $('a[href^="#"]').click(function(){
        var el = $(this).attr('href');
        $('body').animate({
            scrollTop: $(el).offset().top}, 2000);
        return false;
    });
    $('#subscribe').on('change', function() {
        $('#subscribe-area').toggle();
    });






    $(document).ready(function () {
        //i1391nitialize swiper when document ready
        var mySwiper = new Swiper ('.swiper-container', {
            // Optional parameters
            direction: 'horizontal',
            slidesPerView: 'auto',
            centeredSlides: true,
            loop: true,
            paginationClickable: true,
            spaceBetween: 30
        })
    });



    $('#button-menu').click(function(){
        $('.header-dropdown-menu').toggleClass('active');


    });








});









